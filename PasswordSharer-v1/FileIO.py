#Public Domain
#Version 1.9
import os
import bz2
import glob

def FileExists(IncomingFilename):
	return os.path.isfile(IncomingFilename)

def DeleteFile(IncomingFilename):
	os.remove(IncomingFilename)

def WriteCompressedFile(Filename,IncomingBinaryData,WriteMode='wb',CompressionLevel=9):
	try:
		OpenFile = bz2.open(Filename,WriteMode,CompressionLevel)
	except:
		raise ValueError('File could not be created.!')
	
	OpenFile.write(IncomingBinaryData)
	OpenFile.close()

def LoadCompressedFile(Filename,ReadMode='rb'):
	try:
		OpenFile = bz2.open(Filename,ReadMode)
	except:
		raise ValueError('File '+Filename+' not found. Please create one!')
	
	Temp = OpenFile.read()
	OpenFile.close()
	
	return Temp

def DeleteFile(Filename):
	
	if os.path.isfile(Filename):
		os.remove(Filename)
	
def DeleteEmptyDirectory(Dirname):
	os.rmdir(Dirname)

def GlobAllFiles(GlobString):
	return [f for f in glob.glob(GlobString)]
	
def NormaliseSlashes(FullFilename,OptionalForwardSlashToBack):
	
	if OptionalForwardSlashToBack is not None:
		
		return FullFilename.replace("\\","/");
	
	else:
		
		return FullFilename.replace("/","\\")

def DetectSlashes(FullFilename):
	DetectType = 0
	
	if '\\' in FullFilename:
		DetectType += 1
		
	if '/' in FullFilename:
		DetectType += 2

	return DetectType

def ExtractEndFilename(FullFilename):
	
	Normalised = NormaliseSlashes(FullFilename,True);
	
	DetectType = DetectSlashes(Normalised)
	
	if DetectType is 0:
		return Normalised
	elif DetectType is 1:
		return Normalised[Normalised.rindex('\\')+1:]
	elif DetectType is 2:
		return Normalised[Normalised.rindex('/')+1:]
	else:
		raise ValueError('FullFilename slashes were not normalised',FullFilename)	

def ExtractEXT(FullFilename):
	return FullFilename[FullFilename.rindex('.')+1:]

def RemoveEXT(FullFilename):
	return FullFilename[:FullFilename.rindex('.')]

def GetDefaultSlash(FullFilename):
	DetectType = DetectSlashes(FullFilename)
	
	if DetectType is 1:
		return '\\'
	else:
		return '/'


def AppendFileLine(Filename,LineToWrite,AppendNewLine=True):
	try:
		OpenFile = open(Filename,'a')
	except:
		raise ValueError('File could not be created!')
	
	if AppendNewLine:
		OpenFile.writelines(LineToWrite + '\n')
	else:
		OpenFile.writelines(LineToWrite)
	
	OpenFile.close()

def WriteFileLines(Filename,Lines,WriteMode='w',AppendNewLine=True):
	try:
		OpenFile = open(Filename,WriteMode)
	except:
		raise ValueError('File could not be created.!')
	
	for EachLine in Lines:
		if AppendNewLine:
			OpenFile.writelines(EachLine+'\r\n')
		else:
			OpenFile.writelines(EachLine)
	
	OpenFile.close()

def WriteFile(Filename,IncomingData,WriteMode='w'):
	try:
		OpenFile = open(Filename,WriteMode)
	except:
		raise ValueError('File could not be created.!')
	
	OpenFile.write(IncomingData)
	OpenFile.close()
	

def LoadFile(Filename):
	try:
		OpenFile = open(Filename,'r')
	except:
		raise ValueError('File '+Filename+' not found. Please create one!')
	
	Temp = OpenFile.read()
	OpenFile.close()
	return Temp

def LoadFileLines(Filename,StripWhiteSpace=False):

	try:
		OpenFile = open(Filename,'r')
	except:
		raise ValueError('File '+Filename+' not found. Please create one!')
	
	if StripWhiteSpace:
		Lines = [Iter.strip().split('\r\n') for Iter in OpenFile]
	else:
		Lines = [Iter.split('\r\n') for Iter in OpenFile]
	
	OpenFile.close()
	
	NewLines = []
	for EachLine in Lines:
		NewLines += EachLine
	
	return NewLines
	
def SaveDictionary(Filename,IncomingDictionary,SplitChars="|",WriteMode='w'):
	
	try:
		OpenFile = open(Filename,WriteMode)
	except:
		raise ValueError('Filename '+str(Filename)+' could not be created!')
	
	for TempKey in IncomingDictionary.keys():
		OpenFile.writelines(str(TempKey)+"|"+IncomingDictionary[TempKey]+"\r\n")
	
	OpenFile.close()

def LoadDictionary(filename,SplitChars='|'):
	NewLines = LoadFileLines(filename)

	NewDictionary = {}
	
	for EachLine in NewLines:
		Temp = EachLine.split(SplitChars)
		try:
			if Temp is not None:
				if len(Temp) == 2:
					NewDictionary[Temp[0]] = Temp[1].strip()
				else:
					print("WARNING: "+filename+" gave a Temp length not matching 2 (some sort of file formatting issue?)")
			else:
				print("WARNING: "+filename+" gave a Temp was none (some sort of file formatting issue?)")
			
		except:
			raise ValueError('Split error in ' + EachLine + '. Possibly missing a split char? In this case: ' + SplitChars)
			
	return NewDictionary
