import os
import time
import hashlib
import getpass
import string
import random
import binascii
import struct
from FileIO import *
from DiffieHellmanKeyExchange import *
from KeyStoreObject import *
from KeyStoreUI import *
from TemporalHashObscurantum import *

CurrentDirectory = os.path.dirname(os.path.abspath(__file__))
#os.path.join()

#G_KeyStore = KeyStore()
G_KeyStore = KeyStoreUI()

G_PreprendPass = ""
G_PostpendPass = ""
G_BasicSalt = "GlobalSalt"
G_TemporalLevel = 8
	

def RandomString(NumberOfChars, chars = string.ascii_letters + string.digits):
	return ''.join(random.choice(chars) for _ in range(NumberOfChars))

def CryptoSecureRandomInt(ValueCeiling):
	return struct.unpack('I', os.urandom(4))[0] % ValueCeiling


def CheckIfHashesExist(InputDirectory):
	
	Collector = []
	
	ValidExt = ["txt"]
	
	for TempExt in ValidExt:
		
		GlobString = os.path.join(InputDirectory,"*."+TempExt)
		ListOfFiles = GlobAllFiles(GlobString)
		
		for FileInList in ListOfFiles:
		
			return True
		
	return False


def BulkLoadHashes(InputDirectory):
	
	Collector = []
	
	ValidExt = ["txt"]
	
	for TempExt in ValidExt:
		
		GlobString = os.path.join(InputDirectory,"*."+TempExt)
		
		ListOfFiles = GlobAllFiles(GlobString);
	
		for FileInList in ListOfFiles:
			
			Collector += LoadFileLines(FileInList,True)
	
	print(str(len(Collector))+" hashes loaded!")
	
	return Collector
	
	
def HashPass(IncomingPass,IncomingSalt,Iterations=200000):

	TempSalt = TemporalHashObscurantum()

	GenPass = G_PreprendPass+IncomingPass+G_PostpendPass
	GenSalt = IncomingSalt + TempSalt.GetTemporalSaltHashed(G_TemporalLevel)
	
	GenSalt = TempSalt.TemporalHashCycler(GenSalt)
	
	TempSalt.TemporalHashCycler(GenPass+GenSalt)
		
	return TempSalt.TemporalHashCycler(GenPass+GenSalt)
	

def GenerateHashFile(OutputDir,RefDir):
	
	#Generate hashfile
			
	HashCollector = []
	HashDictionary = {}
	
	while True:
		
		while True:
			url = getpass.getpass('Enter password to add or leave blank to quit: ')
			if "|" in url:
				print("Pipe symbol not permitted")
			else:
				break
		
		urllen = len(url)
		
		if urllen > 0:
			
			print("Generating fluff to throw off brute-forcers...")
			
			TempIter = CryptoSecureRandomInt(300)
			
			while TempIter > 0:
				url = RandomString(urllen)
				Hashed = HashPass(url,G_BasicSalt)
				HashDictionary[url] = Hashed
				HashCollector.append(Hashed)
				TempIter = TempIter - 1
			
			Hashed = HashPass(url,G_BasicSalt)
			HashDictionary[url] = Hashed
			HashCollector.append(Hashed)
			print("Fluff generated")
			print("Successfully added!")
			
		else:
			
			while True:
				try:
					Choice = int(input("Would you like me to add a large number of randomised hashes? If so, type a random number (EG 4731), if not, just put the number zero (0):"))
					break
				except:
					print("Warning: Invalid Input. Use only numbers!")
			
			if Choice > 0:
				Iter = 0
				while Iter < Choice:
					url = RandomString(urllen)
					Hashed = HashPass(url,G_BasicSalt)
					HashDictionary[url] = Hashed
					HashCollector.append(Hashed)
					sys.stdout.write("Iterations left: %d%%   \r" % (Choice-Iter) )
					sys.stdout.flush()
					Iter = Iter + 1
			
			FilenameEnd = time.strftime('HashShareFile %Y-%m-%d-%H-%M-%S.txt', time.localtime())
			RefEnd = time.strftime('DO NOT SHARE Reference HashShareFile %Y-%m-%d-%H-%M-%S.txt', time.localtime())
			
			OutputFilename = os.path.join(OutputDir,FilenameEnd)
			WriteFileLines(OutputFilename,HashCollector)
			
			print("Hash file output as: "+OutputFilename)
			
			RefFilename = os.path.join(RefDir,RefEnd)
			SaveDictionary(RefFilename,HashDictionary)
			
			print("[DO NOT SHARE!] Reference Hash file output as: "+RefFilename)
			
			break
			

def CheckForMatches(InputDir):
	#Check for matches
			
	HashesList = BulkLoadHashes(InputDir)
			
	while True:
		url = getpass.getpass('Enter password to check or leave blank to quit: ')
		urllen = len(url)
		
		if urllen > 0:
			
			Hashed = HashPass(url,G_BasicSalt)
			
			HashMatch = False
			
			for Hash in HashesList:
				
				if Hashed == Hash:
					HashMatch = True
					print("The password you input matched!")
					print("Matches hash: "+Hashed)
					print('Show this matching hash to your counterpart')
					break
					
			if HashMatch is False:
				print("This password did not match anything in the selection")
			
		else:
			break
	

if __name__ == '__main__':
	
	
	
	Version = "Password Sharer v2.1 beta"
	InputDir = os.path.join(os.path.dirname(os.path.abspath(__file__)),"InputHashes")
	OutputDir = os.path.join(os.path.dirname(os.path.abspath(__file__)),"OutputHashes")
	RefDir = os.path.join(os.path.dirname(os.path.abspath(__file__)),"ReferenceHashes")
	
	Choice = 0
	
	while Choice != 4:
		
		HashesExist = CheckIfHashesExist(InputDir)
		
		TextScreen = ""     
		
		TextScreen = TextScreen + "====================\r\n"
		TextScreen = TextScreen + "Password Sharer Menu\r\n"
		TextScreen = TextScreen + "====================\r\n\r\n"
		
		TextScreen = TextScreen + "Version: "+Version+"\r\n\r\n"
		TextScreen = TextScreen + "Choose mode:\r\n\r\n"
		TextScreen = TextScreen + "1) Generate Hashfile to Share\r\n"
		
		if HashesExist:
		
			TextScreen = TextScreen + "2) Solve Shared Hashfile\r\n"
		
		TextScreen = TextScreen + "3) Go To KeyShare Menu\r\n"
		TextScreen = TextScreen + "4) Exit Program (Closes Program)\r\n\r\n"
		TextScreen = TextScreen + "Input Number For Option: "
	
		try:
			Choice = int(input(TextScreen))
		except:
			print("Warning: Invalid Input. Use only numbers!")
			Choice = 0
		
		if Choice is 1:
			
			GenerateHashFile(OutputDir,RefDir)
			
		elif Choice is 2 and HashesExist:
			
			CheckForMatches(InputDir)
		
		elif Choice is 3:
			
			G_KeyStore.UI()
			G_KeyStore.Save() 
			
		elif Choice is 4:
			
			pass
		
	
	exit()
