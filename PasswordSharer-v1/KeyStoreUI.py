import base64
from FileIO import *
from TextUI import *
from TemporalSalt import *
from TemporalHashObscurantum import *
from KeyStoreObject import *

CurrentDirectory = os.path.dirname(os.path.abspath(__file__))

class KeyStoreUI:
	
	def __init__(self):
		
		self.KSO = KeyStore()
		self.TUI = TextUI()
		
		self.DisplaySecret = False
	
	def GenerateKeyHandshake(self):
		
		self.TUI.PrintOverwrite("Generating Key Handshake...")
		
		self.TUI.PrintOverwrite("Generating Hash Index...")
		TempHash = self.KSO.GenerateHashIndex()
		
		self.TUI.PrintOverwrite("Generating DiffieHellman Key...")
		TempDiffieHellmanKey = self.KSO.GenerateMatchingDiffieHellmanKey(TempHash)
					
		TempPrivateKey = TempDiffieHellmanKey.GetPrivateKeyAsString()
		TempPublicKey = TempDiffieHellmanKey.GetPublicKeyAsString()
		
		self.TUI.PrintOverwrite("Updating KeyStore Entry...")
		self.KSO.UpdateBasicKeyStoreEntry(TempHash,TempPublicKey,TempPrivateKey)
		
		
		TempDict = {}
		TempDict[TempHash] = TempPublicKey
		
		VisualHash = TempHash[:self.KSO.VisualLength]
		VisualPublicKey = TempPublicKey[:self.KSO.VisualLength]
		
		TempFilename = VisualHash+"_"+VisualPublicKey+"_"+self.KSO.GenerateTimestamp()+"."+self.KSO.ValidExt
		
		TempFullFilename = os.path.join(self.KSO.ExportedKeysDir,TempFilename)
		
		self.TUI.PrintOverwrite("Saving To File...")
		SaveDictionary(TempFullFilename,TempDict)
		
		return TempFullFilename


	def GenerateKeyHandshakeResponse(self,TargetHash):
		
		if self.KSO.BasicKeyStore is None:
			print("BasicKeyStore is not initialised")
			return None
		
		if TargetHash not in self.KSO.BasicKeyStore:
			print("No matching TargetHash entry for: "+TargetHash)
			return None
		
		if self.KSO.BasicKeyStore[TargetHash] is None:
			print("No valid TargetHash entry for: "+TargetHash)
			return None
		
		if self.KSO.BasicKeyStore[TargetHash]["public"] is None:
			print("No matching public key for: "+TargetHash)
			return None
		
		
		TempPublicKey = self.KSO.BasicKeyStore[TargetHash]["public"]
		
		TempDict = {}
		TempDict[TargetHash] = TempPublicKey
		
		VisualHash = TargetHash[:self.KSO.VisualLength]
		VisualPublicKey = TempPublicKey[:self.KSO.VisualLength]
		
		TempFilename = VisualHash+"_"+VisualPublicKey+"_"+self.KSO.GenerateTimestamp()+"."+self.KSO.ValidExt
		
		TempFullFilename = os.path.join(self.KSO.ExportedKeysDir,TempFilename)
		
		SaveDictionary(TempFullFilename,TempDict)
		
		
		return TempFullFilename
	

	def Load(self):
		self.KSO.LoadAll()
	
	def Save(self):
		self.KSO.SaveAll()
	
	def Import(self):
		self.KSO.ImportKeys()
	
	def Export(self):
		return self.GenerateKeyHandshake()
		
		
	def StartHandshake(self):
		
		TempFilename = self.Export()
		
		print("Handshake Generated. Give "+TempFilename+" to the other person you want to establish a handshake with.")
		print("Tell the other person to copy the file you give them (without renaming or modifying it) to their ImportedKeys folder.")
		print("Tell the other person to then pick \"Process Handshake\" when running the program.")
		self.TUI.PressEnterToContinue()
		
	def ProcessHandshake(self):
		
		print("If not already done, please place the shared key file given by the other person into the ImportedKeys folder into: "+self.KSO.ImportedKeysDir)
		self.TUI.PressEnterToContinue()
		
		self.TUI.PrintOverwrite("Importing keys...")
		ListOfImportedFilesAndKeys = self.KSO.ImportKeys()
		
		TempLen = len(ListOfImportedFilesAndKeys)
		
		if TempLen < 1:
			self.TUI.PrintOverwrite("Failed to import any keys.")
			return
		
		self.TUI.PrintOverwrite("Imported. Generating Key Secrets. This may take a while.")
		self.KSO.GenerateImportedKeysSecrets()
		
		
		Iter = 0
		while Iter < TempLen:
			
			TempDict = ListOfImportedFilesAndKeys[Iter]
			TempFilenameA = TempDict["Filename"]
			TempTargetHash = next(iter(TempDict["ImportedKey"]))
			TempImportKey = TempDict["ImportedKey"][TempTargetHash]
			
			TempExportedFilename = self.GenerateKeyHandshakeResponse(TempTargetHash)
		
			if TempExportedFilename is None:
				print("GenerateKeyHandshakeResponse failed")
				return
			
			print("Response generated for imported key file: "+TempFilenameA)
			print("Response can be found at: "+TempExportedFilename)
			print("Give a copy of \'"+ExtractEndFilename(TempExportedFilename)+"\' to the person who sent you \'"+ExtractEndFilename(TempFilenameA)+"\'")
			print("Tell them to use the option \"Finish Handshake\" once they have placed their file in the ImportedKeys folder.")
			
			TempIdentifierKey = TempTargetHash[:self.KSO.VisualLength]+"_"+TempImportKey[:self.KSO.VisualLength]
			
			
			UserInput = ""
			while len(UserInput) < 1:
			
				UserInput = input("\r\nPlease input the name, alias or other memorable information for the person who sent you \'"+ExtractEndFilename(TempFilenameA)+"\'.\r\nIt'll be stored against an identifier key so you can easily reload this later on.\r\nInput Identifier Key name: ")

				 
			#IdentifiedKeyStore:{identifier:hashid_importedkey}
			self.KSO.IdentifiedKeyStore[UserInput] = ""+TempIdentifierKey
			
			Iter = Iter + 1
			
		print("Once you have given the other person a copy of the file, you need to go to \"Select And Load Key Identifier\" in the main menu.")
		self.TUI.PressEnterToContinue()
		
	def FinishHandshake(self):
		
		print("If not already done, please place the shared key file given by the other person into the ImportedKeys folder into: "+self.KSO.ImportedKeysDir)
		self.TUI.PressEnterToContinue()
		
		self.TUI.PrintOverwrite("Importing keys...")
		ListOfImportedFilesAndKeys = self.KSO.ImportKeys()
		
		TempLen = len(ListOfImportedFilesAndKeys)
		
		if TempLen < 1:
			self.TUI.PrintOverwrite("Failed to import any keys.")
			return
		
		self.TUI.PrintOverwrite("Imported. Generating Key Secrets. This may take a while.")
		self.KSO.GenerateImportedKeysSecrets()
		
		Iter = 0
		while Iter < TempLen:
			
			TempDict = ListOfImportedFilesAndKeys[Iter]
			TempFilenameA = TempDict["Filename"]
			TempTargetHash = next(iter(TempDict["ImportedKey"]))
			TempImportKey = TempDict["ImportedKey"][TempTargetHash]
			
			self.TUI.PrintOverwrite("Secret generated for imported key file: "+TempFilenameA)
			
			TempIdentifierKey = TempTargetHash[:self.KSO.VisualLength]+"_"+TempImportKey[:self.KSO.VisualLength]
			
			UserInput = ""
			while len(UserInput) < 1:
			
				UserInput = input("\r\nPlease input the name, alias or other memorable information for the person who sent you \'"+ExtractEndFilename(TempFilenameA)+"\'.\r\nIt'll be stored against an identifier key so you can easily reload this later on.\r\nInput Identifier Key name: ")
				
				 
			#IdentifiedKeyStore:{identifier:hashid_importedkey}
			self.KSO.IdentifiedKeyStore[UserInput] = ""+TempIdentifierKey
			
			Iter = Iter + 1
		
		print("You need to go to \"Select And Load Key Identifier\" in the main menu.")
		self.TUI.PressEnterToContinue()
	
	
	def LoadSecretGeneric(self,IncompleteHashID,IncompleteImportKey):
		
		if len(IncompleteHashID) < 1:
			raise ValueError("IncompleteHashID is less than 1")
			
		if len(IncompleteImportKey) < 1:
			raise ValueError("IncompleteImportKey is less than 1")
		
		ListOfKeys = self.KSO.LookupIdentifierKeys(IncompleteHashID,IncompleteImportKey)
		
		TempIDKey = ""+IncompleteHashID+"_"+IncompleteImportKey
		
		if len(ListOfKeys) < 1:
			print(""+TempIDKey+" was not found.")
			return
		
		
		self.KSO.SelectedKey = ""+IncompleteHashID+"_"+IncompleteImportKey
		self.KSO.SelectedSecret = ""+ListOfKeys[0]["generated"]
		
		print(""+TempIDKey+" successfully loaded")
		
		if self.DisplaySecret:
			print("SecretKey: "+str(self.KSO.SelectedSecret))
	
	
	def LoadSecret(self,IdentifierKeyPair):
		
		if "_" not in IdentifierKeyPair:
			print("Must contain an underscore between the HashID and ImportKey")
			return
		
		KeyPairList = IdentifierKeyPair.split("_")
		
		TempHashID = KeyPairList[0]
		TempImportKey = KeyPairList[1]
		
		self.LoadSecretGeneric(TempHashID,TempImportKey)
	
	
	def SelectAndLoadIdentifierKey(self):
		
		Collector = []
		
		Iter = -1
		print("\r\nSelect Identifier Key:\r\n\r\n"+str(Iter)+") to quit")
		
		Iter = 0
		print(str(Iter)+") to clear currently loaded key")
		
		Collector.append("")
		
		for Identifier in self.KSO.IdentifiedKeyStore.keys():
			
			Iter = Iter + 1
			print(str(Iter)+") "+Identifier+" | "+self.KSO.IdentifiedKeyStore[Identifier])
			Collector.append(self.KSO.IdentifiedKeyStore[Identifier])
	
		if Iter is -1:
			
			print("No identifier keys in memory.")
			return
		
		Choice = -2
		while Choice != -1:
			
			
			try:
				Choice = int(input("Please input a valid number from the above list, or put -1 to quit.\r\nInput option: "))
			except:
				print("Warning: Invalid Input. Use only numbers!")
				Choice = -2
			
			
			if Choice > -1 and Choice <= Iter:
				break
			elif Choice == -1:
				
				print("Quitting")
				return
		
		TempLoadedKey = Collector[Choice]
		
		if TempLoadedKey == "":
			self.KSO.SelectedKey = ""
			self.KSO.SelectedSecret = ""
			
		else:
			self.LoadSecret(TempLoadedKey)
	
	
	def SelectAndLoadIdentifierKeyManually(self):
		
		
		Choice = ""
		
		while Choice == "":
			
			Choice = input("Please input hash key to look up (hashid_importkey EG 0000_0000):")
			self.LoadSecret(Choice)
	
	
	def SelectAndLoadIdentifierKeyOptions(self):
		
		ExitChoice = 4
		Choice2 = 0
			
		while Choice2 is not ExitChoice:
			
			ConsoleText = "\r\nSelect And Load Identifier Key\r\n\r\n"
			ConsoleText = ConsoleText + "======================================\r\n"
			ConsoleText = ConsoleText + "If unsure, choose \"From Internal List\"\r\n"
			ConsoleText = ConsoleText + "======================================\r\n\r\n"
			ConsoleText = ConsoleText + "1) From Internal List\r\n"
			ConsoleText = ConsoleText + "2) Manual Entry\r\n"
			ConsoleText = ConsoleText + "3) Toggle Secret Key Visibility (Currently: "+str(self.DisplaySecret)+")\r\n"
			ConsoleText = ConsoleText +  str(ExitChoice) + ") Go back\r\n"
			ConsoleText = ConsoleText + "Input option: "
			
			try:
				Choice2 = int(input(ConsoleText))
			except:
				print("Warning: Invalid Input. Use only numbers!")
				Choice2 = 0
			
			if Choice2 is 1:
				
				self.SelectAndLoadIdentifierKey()
				
			elif Choice2 is 2:
				
				self.SelectAndLoadIdentifierKeyManually()
				
			elif Choice2 is 3:
				
				self.DisplaySecret = not self.DisplaySecret
				print("Display Secret is now set to \'"+str(self.DisplaySecret)+"\'")
				Choice2 = 0
				
			elif Choice2 is ExitChoice:
				#Go back
				return
	
	
	def ClearKeyStore(self):
		
		print("Are you ABSOLUTELY SURE you wish to delete ALL KEYSTORES? This includes the physical files and what's stored in memory!")
		print("This action CANNOT be undone!\r\n")
		Verify = input("Type YES in all capitals to verify: ")
		
		if Verify == "YES":
			
			self.KSO.ClearKeyStore()
			
			print("All keystores deleted.")
		else:
			print("Cancelled")
			
		return
	
	
	def RecodeHex(self,IncomingHex,NewEncoding=None):
		
		TempBytes = bytes.fromhex(IncomingHex)
	
		if NewEncoding is None:
			return TempBytes
			
		return TempBytes.encode(NewEncoding)
		
	def ForkKey(self):
		
		SettingFolds = 7
		SettingIterations = 122133
		SettingTemporalLevel = 8
		SettingOneTimePadLength = -1
		SettingTimedate = "00000000000000"
		TargetSecret = self.KSO.SelectedSecret
		
		GeneratedFork = ""
		
		Choice = 0
		ExitValue = 9
		
		TS = TemporalSalt()
		THO = TemporalHashObscurantum()
		
		ComparisonZeroes = "0" * TS.LengthCap
		
		while Choice != ExitValue:
			
			TextScreen = "\r\n"
			TextScreen = TextScreen + "=============\r\n"
			TextScreen = TextScreen + "Fork Key Menu\r\n"
			TextScreen = TextScreen + "=============\r\n\r\n"
			
			TextScreen = TextScreen + "1) Set Number Of Folds (Currently: "+str(SettingFolds)+")\r\n"
			TextScreen = TextScreen + "2) Set Number Of Iterations (Currently: "+str(SettingIterations)+")\r\n"
			TextScreen = TextScreen + "3) Set Temporal Precision Level (Currently: "+str(SettingTemporalLevel)+" AKA "+TS.NumberToOption(SettingTemporalLevel)+")\r\n"
			
			if SettingOneTimePadLength < 0:
				TextScreen = TextScreen + "4) Set One-Time-Pad Target Length [Optional] (Not Set)\r\n"
				
			else:
				TextScreen = TextScreen + "4) Set One-Time-Pad Target Length [Optional] (Currently: "+str(SettingOneTimePadLength)+")\r\n"
			
			
			if SettingTimedate != ComparisonZeroes:
				TextScreen = TextScreen + "5) Set Fixed Timedate [Optional] (Currently: "+SettingTimedate+")\r\n"
			
			else:
				TextScreen = TextScreen + "5) Set Fixed Timedate [Optional] (Not Set)\r\n"
		
			
			if TargetSecret == "":
				TextScreen = TextScreen + "6) Input Custom Password [Required] (Currently Not Set)"
				
			else:
				TextScreen = TextScreen + "6) Input Custom Password (Currently Set)"
				
			
			if self.CheckIfSecretLoaded():
				if self.SelectedSecret == TargetSecret:
					TextScreen = TextScreen + " [Shared Secret Loaded]"
		
			TextScreen = TextScreen + "\r\n"
		
			if TargetSecret != "":
				TextScreen = TextScreen + "7) Generate Forked Key\r\n"
			
			if GeneratedFork != "":
				TextScreen = TextScreen + "8) Reshow Forked Key\r\n"
			
			TextScreen = TextScreen + str(ExitValue) + ") Go Back To KeyShare Menu\r\n\r\n"
			TextScreen = TextScreen + "Input Number For Option: "
			
			Choice = self.GetUserInputIntSafe(TextScreen)
			
			print("")
			
			if Choice is 1:
				
				TempFolds = self.TUI.GetUserInputIntSafe("Specify a number of folds greater than 0 [4 Recommended]: ")
				
				if TempFolds < 1:
					TempFolds = 0
					
				SettingFolds = TempFolds
				
			elif Choice is 2:
				
				TempIters = self.TUI.GetUserInputIntSafe("Specify a number of iterations greater than 0 [At least 100000 recommended]: ")
				
				if TempIters < 1:
					TempIters = 1
					
				SettingIterations = TempIters
				
			elif Choice is 3:
			
				TempText = ""
			
				TS.PrintConversionList()
			
				TempLen = (len(TS.ConversionList) - 1)
			
				TempText = TempText + "Specify a Temporal Precision Level as a number between 0 (Off) and "+str(TempLen)+" (Second Precision) [8 is recommended]: "
			
				TempLevel = self.TUI.GetUserInputIntSafe(TempText)
				
				if TempLevel < 0:
					TempLevel = 0
				
				elif TempLevel > TempLen:	
					TempLevel = TempLen
					
				SettingTemporalLevel = TempLevel
				
			
			elif Choice is 4:
				
				TempOTP = self.TUI.GetUserInputIntSafe("Specify the length of key to return (set to -1 if you wish to have a dynamic length).\r\n[Recommended: -1]: ")
				
				if TempOTP < -1:
					TempOTP = -1
					
				SettingOneTimePadLength = TempOTP
				
			elif Choice is 5:
				
				TempTimedate = self.TUI.GetUserInputIntSafe("Input a value representing YYYYMMDDHHMMSS [EG 20210304114310].\r\nAny numbers missing will be filled out with zeroes [EG 20210308 will become 20210308000000]: ")
				
				if TempTimedate < 0:
					TempOTP = 0
					
				TTD = str(TempTimedate)
				
				if len(TTD) > TS.LengthCap:
					TTD = TTD[:TS.LengthCap]
				
				if len(TTD) < TS.LengthCap:
					
					TempZeroes = "0" * (TS.LengthCap - len(TTD))
					TDD = TTD + TempZeroes
				
						
			elif Choice is 6:
				
				TargetSecret = self.TUI.GetUserPasswordSafe("Specify replacement password, or leave blank to reload Shared Secret (if available): ")
				
				if TargetSecret == "":
					TargetSecret = self.KSO.SelectedSecret
		
				
			elif Choice is 7 and TargetSecret != "":
			 
				#SettingFolds = 4
				#SettingIterations = 122133
				#SettingTemporalLevel = 8
				#SettingOneTimePadLength = -1
				#SettingTimedate = "00000000000000"
				#TargetSecret = self.SelectedSecret
				
				
				if SettingTimedate == ComparisonZeroes and SettingTemporalLevel > 0:
					TempTimedate = None
				
				else:
					TempTimedate = SettingTimedate
				
				print("Generating. Please wait. This may take several minutes to make brute forcing computationally costly.")
						
				if SettingOneTimePadLength > 0:	
					GeneratedFork = THO.TemporalOneTimePadGenerator(TargetSecret, SettingOneTimePadLength, SettingFolds, None, SettingTemporalLevel, TempTimedate)
					
				else:
					GeneratedFork = THO.TemporalHashCycler(TargetSecret, SettingFolds, SettingOneTimePadLength, SettingIterations, SettingTemporalLevel, TempTimedate)
				
				GeneratedBytes = self.RecodeHex(GeneratedFork,None)
				
				Choice = 8
				
			
			if Choice is 8 and GeneratedFork != "":
				
				print("\r\nThese are all the same key represented differently (to meet different password requirements): \r\n")
				
				print("Base16:\t"+GeneratedFork)
				print( "Base32:\t"+base64.b32encode(GeneratedBytes).decode() )
				print( "Base64:\t"+base64.b64encode(GeneratedBytes).decode() )
				print( "Base85:\t"+base64.b85encode(GeneratedBytes).decode() )
				
				print("")
				self.PressEnterToContinue()
			
			if Choice is ExitValue:
				return
		
	def UI(self):
		
		self.Load()
		
		while True:
		
			CheckImportKeys = self.KSO.CheckIfImportKeysExist()
			CheckIdentifierKeys = self.KSO.CheckIfIdentifierKeysExist()
		
			ConsoleText = ""
			
			ConsoleText = ConsoleText + "==================\r\n"
			ConsoleText = ConsoleText + "KeyShare Main Menu\r\n"
			ConsoleText = ConsoleText + "==================\r\n\r\n"
			
			ConsoleText = ConsoleText + "Version: Secret Sharing Protocol v2.2\r\n\r\n"
			
			ConsoleText = ConsoleText + "--> If you're not sure what to do, you want to pick Start Handshake <--\r\n\r\n"
			
			ConsoleText = ConsoleText + "1) Start Handshake\r\n"
			
			if CheckImportKeys:
			
				ConsoleText = ConsoleText + "2) Process Handshake\r\n"
				ConsoleText = ConsoleText + "3) Finish Handshake\r\n"
			
			if CheckIdentifierKeys:
			
				ConsoleText = ConsoleText + "4) Select And Load Identifier Key\r\n"
			
			ConsoleText = ConsoleText + "5) Delete ALL KeyStores\r\n"
			ConsoleText = ConsoleText + "6) Create Forked Key\r\n"
			
			ConsoleText = ConsoleText + "7) Go Back To Main Menu\r\n\r\n"
			ConsoleText = ConsoleText + "Input Number For Option: "
			
			try:
				Choice = int(input(ConsoleText))
			except:
				print("Warning: Invalid Input. Use only numbers!")
				Choice = 0
			
			if Choice is 1:
				
				self.StartHandshake()
				self.Save()
				
			elif Choice is 2 and CheckImportKeys:
				
				self.ProcessHandshake()
				self.Save()
				
			elif Choice is 3 and CheckImportKeys:
				
				self.FinishHandshake()
				self.Save()
				
			elif Choice is 4 and CheckIdentifierKeys:
				
				self.SelectAndLoadIdentifierKeyOptions()
			
			elif Choice is 5:
				
				self.ClearKeyStore()
			
			elif Choice is 6:
				
					self.ForkKey()
			
			elif Choice is 7:
				return
				
			else:
				
				print("Invalid option!")
	
		self.Save()
