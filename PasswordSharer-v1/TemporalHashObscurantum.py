from HashObscurantum import *
from TemporalSalt import *


class TemporalHashObscurantum:
	
	
	def __init__(self,DefaultIterations=112421):
		
		self.InternalHashObscurantum = HashObscurantum(DefaultIterations)
		self.InternalTemporalSalt = TemporalSalt()
		
		
		NormalLevel = self.InternalTemporalSalt.NumberToOption(7)
		self.NormalLevel = self.InternalTemporalSalt.ConversionList[NormalLevel]
		
		self.Iterations = DefaultIterations


	def GetTemporalSaltHashed(self,Level=None,OptionalOverrideDateTime=None):
		
		print("Getting Temporal Salt Hash:",end='\r')
		
		TempSalt = self.InternalTemporalSalt.GetTemporalSalt(Level,OptionalOverrideDateTime)
		
		return self.InternalHashObscurantum.ObscurantumHashCycler(TempSalt)

	def GetTemporalSaltHashedAsInt(self,Level=None,OptionalOverrideDateTime=None):
		
		TempHash = self.GetTemporalSaltHashed(Level,OptionalOverrideDateTime)
		return int(TempHash, 16)


	def TemporalHashCycler(self,IncomingString, MaxFolds=None, MaxLength=50, OverrideIterationAmount=None,OverrideLevel=None,OverrideDatetime=None):
		
		TempLevel = OverrideLevel
		
		if OverrideLevel is None:
			TempLevel = self.NormalLevel
		
		TempIterations = OverrideIterationAmount
		
		if OverrideIterationAmount is None:
			TempIterations = self.Iterations
			
		TemporalKey = self.InternalTemporalSalt.GetTemporalSalt(TempLevel,OverrideDatetime)
			
		HashedTemporalKey = self.InternalHashObscurantum.ObscurantumHashCycler(TemporalKey, MaxFolds, MaxLength, TempIterations)
		
		return self.InternalHashObscurantum.ObscurantumHashCycler(IncomingString+HashedTemporalKey, MaxFolds, MaxLength, TempIterations)

	
	def TemporalOneTimePadGenerator(self,IncomingKey,MinimumOTPLength,TargetFolds=4,TargetSubLength=None,OverrideLevel=None,OverrideDatetime=None):
		
		TempLevel = OverrideLevel
		
		if OverrideLevel is None:
			TempLevel = self.NormalLevel
		
		TemporalKey = self.InternalTemporalSalt.GetTemporalSalt(TempLevel,OverrideDatetime)
		
		HashedTemporalKey = self.InternalHashObscurantum.ObscurantumHashCycler(TemporalKey,TargetFolds,TargetSubLength,self.Iterations)
					
		return self.InternalHashObscurantum.ObscurantumOneTimePadGenerator(IncomingKey+HashedTemporalKey,MinimumOTPLength,TargetFolds,TargetSubLength)
