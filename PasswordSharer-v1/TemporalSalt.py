import time


class TemporalSalt:
	
	def __init__(self,IncomingLevel=8):
		
		self.ConversionList = {
								"NullDate":0, #No date is specified
								"Millennium":1, #Have to agree the key exchange within 1000 years
								"Century":2, #Have to agree the key exchange within 100 years
								"Decade":3, #Have to agree the key exchange within 10 years
								"Year":4, #Have to agree the key exchange within 1 year
								"MonthTenth":5, #Have to agree the key exchange either between months 01-09, or 10-12
								"MonthDigit":6, #Have to agree the key exchange the same month
								"DayTenth":7, #Have to agree the key exchange either between days 01-09, 10-19, 20-29, or 30-31
								"DayDigit":8, #Have to agree the key exchange the same day (UTC)
								"HourTenth":9, #Have to agree the key exchange either between hours 00-09, 10-19, or 20-23
								"HourDigit":10, #Have to agree the key exchange the same hour
								"MinuteTenth":11, #Have to agree the key exchange either between minutes 00-09, 10-19, 20-29, 30-39, 40-49, or 50-59
								"MinuteDigit":12, #Have to agree the key exchange in the same minute
								"SecondTenth":13, #Have to agree the key exchange in the same seconds  00-09, 10-19, 20-29, 30-39, 40-49, or 50-59
								"SecondDigit":14  #Have to agree the key exchange in the same second
								}
		
		self.NumericConversionList = {}
		self.LoadNumericConversionList()
		
		self.LengthCap = len(self.ConversionList) - 1
		self.DefaultLevel = IncomingLevel
	
	
	def LoadNumericConversionList(self):
		
		self.NumericConversionList = {}
		
		for Key in self.ConversionList.keys():
			
			self.NumericConversionList[str(self.ConversionList[Key])] = Key
	
	
	def NumberToOption(self,IncomingNumber):
		
		return self.NumericConversionList[str(IncomingNumber)]
	
	
	def PrintConversionList(self):
		
		TextOutput = ""
		Iter = 0
		
		while Iter < len(self.NumericConversionList):
			
			StrIter = str(Iter)
			
			TextOutput = TextOutput + StrIter+") "+self.NumericConversionList[StrIter] + "\t\t"
			
			if len(self.NumericConversionList[StrIter]) < 5:
				TextOutput = TextOutput + "\t"
			
			if (Iter%3) == 2:
				 TextOutput = TextOutput + "\r\n"
			
			Iter = Iter + 1

		print(TextOutput)
	
	
	def GetTemporalSalt(self,Level=None,OptionalOverrideDateTime=None):
	
		TempLevel = 9
	
		if Level is None:
			TempLevel = self.DefaultLevel
		else:
			TempLevel = int(Level)
		
		if OptionalOverrideDateTime is None:
			
			TemporalString = str(time.strftime('%Y%m%d%H%M%S', time.localtime()))
			return self.GetTemporalSaltFromDateTime(TempLevel,TemporalString)
		
		else:
			
			return self.GetTemporalSaltFromDateTime(TempLevel,OptionalOverrideDateTime)
		
	
	def GetTemporalSaltAsInt(self,Level=None,OptionalOverrideDateTime=None):
		
		return int(self.GetTemporalSalt(level,OptionalOverrideDateTime))
	
	
	def GetTemporalSaltFromDateTime(self,Level,DateTimeAsString):
		
		TempLevel = int(Level)
		
		if TempLevel > ( len(self.ConversionList) - 1 ):
			TempLevel = len(self.ConversionList) - 1
		
		if TempLevel < 0:
			TempLevel = 0
			
		TempString = ""+DateTimeAsString
		
		if len(TempString) < self.LengthCap:
		
			ZeroString = "0" * (self.LengthCap - len(TempString))
			TempString = TempString + ZeroString
		
		elif len(TempString) > self.LengthCap:
		
			TempString = TempString[:self.LengthCap]
		
		
		Calc = len(TempString) - TempLevel
		ZeroString = "0" * Calc
		
		return DateTimeAsString[:TempLevel] + ZeroString
