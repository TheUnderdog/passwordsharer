import os
import time
import copy
import base64
import string
import random
import hashlib
import getpass
from FileIO import *
from TextUI import *
from TemporalSalt import *
from HashObscurantum import *
from TemporalHashObscurantum import *
from DiffieHellmanKeyExchange import *

CurrentDirectory = os.path.dirname(os.path.abspath(__file__))

class KeyStore:
	
	def __init__(self):
		
		self.TUI = TextUI()
		
		#For Private and Public keys
		#BasicKeyStore:{hash:{public:private:}}
		self.BasicKeyStore = {}
		
		#For Keys generated from imported keys
		#GeneratedKeyStore:{hash:[{imported:generated:}]}
		self.GeneratedKeyStore = {}
		
		#For Imported Keys
		#ImportedKeys:{hash:[imported]}
		self.ImportedKeys = {}
		
		#For shared key lookups
		#IdentifiedKeyStore:{identifier:hashid_importedkey}
		self.IdentifiedKeyStore = {}
		
		self.KeyStoreDir = os.path.join(os.path.dirname(os.path.abspath(__file__)),"KeyStore")
		
		self.BasicKeyStoreFilename = os.path.join(self.KeyStoreDir,"DO-NOT-SHARE-Private-KeyStore.key")
		self.GeneratedKeyStoreFilename = os.path.join(self.KeyStoreDir,"DO-NOT-SHARE-Generated-KeyStore.key")
		self.ImportedKeyStoreFilename = os.path.join(self.KeyStoreDir,"DO-NOT-SHARE-Imported-KeyStore.key")
		self.IdentifierKeyStoreFilename = os.path.join(self.KeyStoreDir,"DO-NOT-SHARE-Identifier-KeyStore.key")
		
		self.ImportedKeysDir = os.path.join(os.path.dirname(os.path.abspath(__file__)),"ImportedKeys")
		self.ExportedKeysDir = os.path.join(os.path.dirname(os.path.abspath(__file__)),"ShareKeys")
		
		self.SelectedKey = ""
		self.SelectedSecret = ""
		
		self.ValidExt = "key"
		
		self.Iterations = 144444
		self.VisualLength = 4
		
		self.TemporalSaltObject = TemporalSalt()
		
		self.GenSalt = ""+self.GenerateTimestamp()
	

	def ClearKeyStore(self):
		
		DeleteFile(self.BasicKeyStoreFilename)
		DeleteFile(self.GeneratedKeyStoreFilename)
		DeleteFile(self.ImportedKeyStoreFilename)
		DeleteFile(self.IdentifierKeyStoreFilename)
		
		self.BasicKeyStore = {}
		self.GeneratedKeyStore = {}
		self.ImportedKeys = {}
		self.IdentifiedKeyStore = {}
		
		self.SelectedKey = ""
		self.SelectedSecret = ""

	def GenerateRandomString(self, NumberOfChars, chars = string.ascii_letters + string.digits):
		return ''.join(random.choice(chars) for _ in range(NumberOfChars))

	
	def GenerateHashIndex(self):

		RandomString = self.GenerateRandomString(30)
		
		Hashed = hashlib.md5(RandomString.encode()).hexdigest()
		Hashed = hashlib.pbkdf2_hmac('sha256', Hashed.encode('ascii') , self.GenSalt.encode('ascii') , self.Iterations).hex()
		
		return Hashed
		
	def GetTemporalSalt(self,Level=9):
	
		TempLevel = Level
		
		if TempLevel < 1:
			TempLevel = 1
		
		TemporalString = time.strftime('%Y%m%d%H%M%S', time.localtime())
		ZeroString = "0" * TempLevel
		
		TemporalString = TemporalString[:len( TemporalString-(TempLevel-1) )] + ZeroString
		
		return TemporalString


	def GenerateTimestamp(self):
		
		return time.strftime('%Y-%m-%d-%H-%M-%S', time.localtime())

	
	def SaveBasicKeyStoreGeneric(self,TargetFilename,CharSeparator="|"):
		
		Collector = {}
		
		#BasicKeyStore:{hash:{public:private:}}
		#hash_type
		
		for TempHash in self.BasicKeyStore.keys():
			
			Collector[TempHash+"_public"] = self.BasicKeyStore[TempHash]["public"]
			Collector[TempHash+"_private"] = self.BasicKeyStore[TempHash]["private"]

		
		if Collector:
			SaveDictionary(TargetFilename,Collector,CharSeparator)
		
		
	def SaveGeneratedKeysGeneric(self,TargetFilename,CharSeparator="|"):
		
		Collector = {}
		
		#GeneratedKeyStore:{hash:[{imported:generated:}]}
		#hash_iter_type
		
		for TempHash in self.GeneratedKeyStore.keys():
			
			TempList = self.GeneratedKeyStore[TempHash]
			
			Iter = 0
			
			while Iter < len(TempList):
				
				Collector[TempHash+"_"+str(Iter)+"_imported"] = self.GeneratedKeyStore[TempHash][Iter]["imported"]
				Collector[TempHash+"_"+str(Iter)+"_generated"] = self.GeneratedKeyStore[TempHash][Iter]["generated"]
				
				Iter = Iter + 1
				
		if Collector:
			SaveDictionary(TargetFilename,Collector,CharSeparator)
	
	
	def SaveImportedKeysGeneric(self,TargetFilename,CharSeparator="|"):
		
		Collector = {}
		
		#ImportedKeys:{hash:[imported]}
		
		for TempHash in self.ImportedKeys.keys():
			
			TempList = self.ImportedKeys[TempHash]
			
			Iter = 0
			while Iter < len(TempList):
				
				Collector[TempHash+"_"+str(Iter)] = self.ImportedKeys[TempHash][Iter]
				Iter = Iter + 1
		
		
		if Collector:
			SaveDictionary(TargetFilename,Collector,CharSeparator)
	
	def SaveIdentifiedKeyStoreGeneric(self,TargetFilename,CharSeparator="|"):
		
		if self.IdentifiedKeyStore:
			#IdentifiedKeyStore:{identifier:hashid_importedkey}
			SaveDictionary(TargetFilename,self.IdentifiedKeyStore,CharSeparator)
		
	def SaveAll(self):
		
		self.SaveBasicKeyStoreGeneric(self.BasicKeyStoreFilename)
		self.SaveGeneratedKeysGeneric(self.GeneratedKeyStoreFilename)
		self.SaveImportedKeysGeneric(self.ImportedKeyStoreFilename)
		self.SaveIdentifiedKeyStoreGeneric(self.IdentifierKeyStoreFilename)
	
	def LoadBasicKeyStoreGeneric(self,TargetFilename,CharSeparator="|"):
		
		self.BasicKeyStore = {}
		
		if not FileExists(TargetFilename):
			return
		
		TempDict = LoadDictionary(TargetFilename,CharSeparator)
		
		#BasicKeyStore:{hash:{public:private:}}
		
		for TempKey in TempDict.keys():
			
			#hash_type
			
			StringList = TempKey.split('_')
			TempHash = StringList[0]
			TempType = StringList[1]
			
			if TempType != "public" and TempType != "private":
				#Insert raise statement
				pass
			
			if TempHash not in self.BasicKeyStore:
				
				self.BasicKeyStore[TempHash] = {}
			
			if self.BasicKeyStore[TempHash] is None:
		
				self.BasicKeyStore[TempHash] = {}
			
			self.BasicKeyStore[TempHash][TempType] = TempDict[TempKey]
		
		
	def LoadGeneratedKeyStoreGeneric(self,TargetFilename,CharSeparator="|"):
		
		if not FileExists(TargetFilename):
			return
		
		self.GeneratedKeyStore = {}
		
		TempDict = LoadDictionary(TargetFilename,CharSeparator)
		
		#GeneratedKeyStore:{hash:[{imported:generated:}]}
		
		for TempKey in TempDict.keys():
			
			#hash_iter_type
			
			StringList = TempKey.split('_')
			
			TempHash = StringList[0]
			TempIter = int(StringList[1])
			TempType = StringList[2]
			
			if self.GeneratedKeyStore is None:
			
				self.GeneratedKeyStore = {}
				
			if TempHash not in self.GeneratedKeyStore:
				
				self.GeneratedKeyStore[TempHash] = []
		
			if self.GeneratedKeyStore[TempHash] is None:
				
				self.GeneratedKeyStore[TempHash] = []
				
			while len(self.GeneratedKeyStore[TempHash]) <= TempIter:
			
				self.GeneratedKeyStore[TempHash].append({})
			
			self.GeneratedKeyStore[TempHash][TempIter][TempType] = TempDict[TempKey]
		
	
	def LoadImportedKeysGeneric(self,TargetFilename,CharSeparator="|"):
		
		print("LOADIMPORTEDKEYSGENERIC")
		
		if not FileExists(TargetFilename):
			return
		
		self.ImportedKeys = {}
		
		TempDict = LoadDictionary(TargetFilename,CharSeparator)
		
		#ImportedKeys:{hash:[imported]}
		
		DebugIter = 0
		#For each entry in the TempDict
		for TempKey in TempDict.keys():
			
			DebugIter = DebugIter + 1
			
			#hash_iter
			
			#Split the hash and iter
			StringList = TempKey.split('_')
			
			
			TempHash = StringList[0]
			TempIter = int(StringList[1])
			
			if TempHash not in self.ImportedKeys:
				
				self.ImportedKeys[TempHash] = []
			
			if self.ImportedKeys[TempHash] is None:
				
				self.ImportedKeys[TempHash] = []
			
			self.ImportedKeys[TempHash].append(TempDict[TempKey]);
			#print("IKTHDBI: "+str(len(self.ImportedKeys[TempHash])))
			
		print("DBI: "+str(DebugIter))
		print(len(self.ImportedKeys))

	
	def LoadIdentifiedKeyStoreGeneric(self,TargetFilename,CharSeparator="|"):
		
		if not FileExists(TargetFilename):
			return
		
		self.IdentifiedKeyStore = {}
		
		#IdentifiedKeyStore:{identifier:hashid_importedkey}
		self.IdentifiedKeyStore = LoadDictionary(TargetFilename,CharSeparator)
	
	
	def LoadAll(self):
		
		self.LoadBasicKeyStoreGeneric(self.BasicKeyStoreFilename)
		self.LoadGeneratedKeyStoreGeneric(self.GeneratedKeyStoreFilename)
		self.LoadImportedKeysGeneric(self.ImportedKeyStoreFilename)
		self.LoadIdentifiedKeyStoreGeneric(self.IdentifierKeyStoreFilename)
			
	
	def ImportKeysGeneric(self,TargetDirectory,CharSeperator="|"):
		
		Collector = []
		ReturnCollector = []
	
		ValidExt = [self.ValidExt]
		
		#Collect a list of imported key files
		for TempExt in ValidExt:
			
			GlobString = os.path.join(self.ImportedKeysDir,"*."+TempExt)
			
			ListOfFiles = GlobAllFiles(GlobString);
		
			for FileInList in ListOfFiles:
				
				TempIncomingDict = LoadDictionary(FileInList,CharSeperator)
				
				if TempIncomingDict is not None:
					if TempIncomingDict:
						Collector.append(TempIncomingDict)
						
						ReturnDict = {}
						ReturnDict["Filename"] = FileInList
						ReturnDict["ImportedKey"] = TempIncomingDict
						
						ReturnCollector.append(copy.deepcopy(ReturnDict))
						
						DeleteFile(FileInList)
				
				
		
		#Check to ensure the ImportedKey storage is in a valid format
		if self.ImportedKeys is None:
			self.ImportedKeys = {}
		
		#Convert the imported keys into the imported keys format for storage
		Iter = 0
		while Iter < len(Collector):
			
			TempDict = Collector[Iter]
			
			for TempHash in TempDict.keys():
			
				#ImportedKeys:{hash:[imported]}
				
				if TempHash not in self.ImportedKeys:
					
					self.ImportedKeys[TempHash] = []
				
				if self.ImportedKeys[TempHash] is None:
					
					self.ImportedKeys[TempHash] = []
				
				
				self.ImportedKeys[TempHash].append(TempDict[TempHash])
			
			Iter = Iter + 1
		
		return ReturnCollector
	
	def ImportKeys(self):
		
		return self.ImportKeysGeneric(self.ImportedKeysDir)
		
	#Creates a DiffieHellman key, either from scratch, or using specified arguments
	def CreateDiffieHellmanKeySetup(self,PublicKey=None,PrivateKey=None):
		
		a = DiffieHellmanKeyExchange()
		
		if PrivateKey is None:
			self.TUI.PrintOverwrite("Generating Private Key")
			a.GeneratePrivateKey()
		else:
			a.SetPrivateKeyFromString(PrivateKey)
			
		if PublicKey is None:
			self.TUI.PrintOverwrite("Generating Public Key")
			a.GeneratePublicKey()
		else:
			a.SetPublicKeyFromString(PublicKey)

		return a
	
	def HasMatchingBasicKeyStoreHash(self,IncomingHash):
		
		if IncomingHash is None:
			return False
			
		if IncomingHash not in self.BasicKeyStore:
			return False
		
		if self.BasicKeyStore[IncomingHash] is None:
			return False
			
		if "public" not in self.BasicKeyStore[IncomingHash]:
			return False
			
		if self.BasicKeyStore[IncomingHash]["public"] is None:
			return False
		
		if "private" not in self.BasicKeyStore[IncomingHash]:
			return False
			
		if self.BasicKeyStore[IncomingHash]["private"] is None:
			return False
			
		return True
	
	#Generates a DiffieHellman key that matches the requested HashID
	def GenerateMatchingDiffieHellmanKey(self,IncomingHash):
		
		#BasicKeyStore:{hash:{public:private:}}
		#self.BasicKeyStore[TempHash][Type]
		
		TempPublic = None
		TempPrivate = None
		
		if IncomingHash is not None:
		
			if IncomingHash in self.BasicKeyStore:
				TempPublic = self.BasicKeyStore[IncomingHash]["public"]
				TempPrivate = self.BasicKeyStore[IncomingHash]["private"]
		
		return self.CreateDiffieHellmanKeySetup(TempPublic,TempPrivate)
	
	
	def UpdateBasicKeyStoreEntry(self,TargetHash,PublicKey,PrivateKey):
		
		self.BasicKeyStore[TargetHash] = {}
		
		self.BasicKeyStore[TargetHash]["public"] = PublicKey;
		self.BasicKeyStore[TargetHash]["private"] = PrivateKey;
				
	def FindImportedKeyInGeneratedKeyStore(self,TargetHash,ImportedKey,CaseSensitive=True):
		
		if TargetHash not in self.GeneratedKeyStore:
			return -3
		
		if self.GeneratedKeyStore[TargetHash] is None:
			return -2
			
		TempLen = len(self.GeneratedKeyStore[TargetHash])
		
		if TempLen < 1:
			return -1
		
		TempImportedKey = ImportedKey
		
		if not CaseSensitive:
			TempImportedKey = ImportedKey.lower()
		
		
		Iter = 0
		while Iter < TempLen:
			
			if TargetHash in self.GeneratedKeyStore:
				
				if self.GeneratedKeyStore[TargetHash] is not None:
			
					if CaseSensitive:
						if TempImportedKey == self.GeneratedKeyStore[TargetHash][Iter]:
							return Iter
							
					else:
						if TempImportedKey == self.GeneratedKeyStore[TargetHash][Iter].lower():
							return Iter
				
			Iter = Iter + 1
		
		return -3
		
		
	def IsImportedKeyInGeneratedKeyStore(self,TargetHash,ImportedKey,CaseSensitive=True):
		
		return (self.FindImportedKeyInGeneratedKeyStore(TargetHash,ImportedKey,CaseSensitive) > -1)
			
	
	def GenerateImportedKeysSecrets(self):
		
		#GeneratedKeyStore:{hash:[{imported:generated:}]}
		#self.GeneratedKeyStore = {}
		
		#ImportedKeys:{hash:[imported]}
		#self.ImportedKeys
		
		#Loop through every hash in the ImportedKeys dictionary
		for TempHash in self.ImportedKeys.keys():
			
			if self.ImportedKeys[TempHash] is not None:
				
			
				#Loop through every imported key under that hash 
				Iter = (len(self.ImportedKeys[TempHash]) - 1)
				while Iter > -1:
					
					self.TUI.PrintOverwrite("Looping through imported keys. "+str(Iter+1)+" remain.")
					
					TempImportedHash = self.ImportedKeys[TempHash][Iter]
					
					TempDiffieHellmanKey = self.GenerateMatchingDiffieHellmanKey(TempHash)
					
					#Check if the DiffieHellmanKey we requested is not already stored in memory
					#And add it if it isn't
					if not self.HasMatchingBasicKeyStoreHash(TempHash):
						
						TempPrivateKey = TempDiffieHellmanKey.GetPrivateKeyAsString()
						TempPublicKey = TempDiffieHellmanKey.GetPublicKeyAsString()
						
						self.UpdateBasicKeyStoreEntry(TempHash,TempPublicKey,TempPrivateKey)
					
					
					#Check imported key doesn't already have a secret generated against it
					if not self.IsImportedKeyInGeneratedKeyStore(TempHash, TempImportedHash):
						
						TempGeneratedKey = TempDiffieHellmanKey.GenerateSecret(TempImportedHash)
						
						#GeneratedKeyStore:{hash:[{imported:generated:}]}
						
						if TempHash not in self.GeneratedKeyStore:
							self.GeneratedKeyStore[TempHash] = []
						
						if self.GeneratedKeyStore[TempHash] is None:
							self.GeneratedKeyStore[TempHash] = []
						
					
						self.GeneratedKeyStore[TempHash].append({})
						EndIter = len(self.GeneratedKeyStore[TempHash]) - 1
						
						self.GeneratedKeyStore[TempHash][EndIter]["imported"] = TempImportedHash;
						self.GeneratedKeyStore[TempHash][EndIter]["generated"] = TempGeneratedKey;
					
					
					Iter = Iter - 1


	def CompareStringN(self,CompareA,CompareB,IncomingN,CaseSensitive=False):
		
		TempA = CompareA
		TempB = CompareB
		
		if not CaseSensitive:
		
			TempA = CompareA.lower()
			TempB = CompareB.lower()
		
		else:
			
			TempA = CompareA
			TempB = CompareB
		
		Iter = 0
		for CharA, CharB in zip(CompareA, CompareB):
			
			if Iter >= IncomingN:
				return True
				
			if CharA != CharB:
				return False
			
			Iter = Iter + 1
			
		return True


	def LookupBasicKeyStore(self,IncompleteHash):
		
		if self.BasicKeyStore is {}:
			
			return []
			
		Collector = []
			
		TempLen = len(IncompleteHash)
		for TempHash in self.BasicKeyStore.keys():
			
			if self.CompareStringN(IncompleteHash,TempHash,TempLen):
				
				Collector.append(TempHash)
	
		return Collector
	
	def LookupGeneratedKeyStore(self,IncompleteHash):
		
		if not self.GeneratedKeyStore:
			
			return []
			
		Collector = []
			
		TempLen = len(IncompleteHash)
		for TempHash in self.GeneratedKeyStore.keys():
			
			if self.CompareStringN(IncompleteHash,TempHash,TempLen):
				
				Collector.append(TempHash)
	
		return Collector
	
	def LookupImportedKeys(self,IncompleteHash):
		
		if not self.ImportedKeys:
			
			return []
			
		Collector = []
			
		TempLen = len(IncompleteHash)
		for TempHash in self.ImportedKeys.keys():
			
			if self.CompareStringN(IncompleteHash,TempHash,TempLen):
				
				Collector.append(TempHash)
	
		return Collector
		
		
	def LookupHashGeneral(self,IncompleteHash):
		
		TempDict = {}
		
		TempDict["BasicKeyStore"] = self.LookupBasicKeyStore(IncompleteHash)
		TempDict["GeneratedKeyStore"] = self.LookupGeneratedKeyStore(IncompleteHash)
		TempDict["ImportedKeys"] = self.LookupImportedKeys(IncompleteHash)
		
		return TempDict
	
	def LookupIdentifierKeys(self,IncompleteHashID,IncompleteImportKey, CaseSensitive=False):
		
		Collector = []
		
		TempIncompleteHashID = ""
		if CaseSensitive:
			TempIncompleteHashID = IncompleteHashID
			
		else:
			TempIncompleteHashID = IncompleteHashID.lower()
		
		TempIncompleteImportKey = ""
		if CaseSensitive:
			TempIncompleteImportKey = IncompleteImportKey
			
		else:
			TempIncompleteImportKey = IncompleteImportKey.lower()
			
			
		#GeneratedKeyStore:{hash:[{imported:generated:}]}
		
		HashIDLen = len(TempIncompleteHashID)
		ImportLen = len(TempIncompleteImportKey)
		
		for GenHash in self.GeneratedKeyStore.keys():
			
			if self.CompareStringN(TempIncompleteHashID,GenHash,HashIDLen,CaseSensitive):
				
				for ImportKeyInList in self.GeneratedKeyStore[GenHash]:
				
					TempImportKeyA = ImportKeyInList["imported"]
				
					if self.CompareStringN(TempIncompleteImportKey,TempImportKeyA,ImportLen,CaseSensitive):
						Collector.append(ImportKeyInList)
		
		return Collector
		
	def CheckIfSecretLoaded(self):
		
		return self.SelectedKey != "" and self.SelectedSecret != ""
		
		
	def CheckIfImportKeysExist(self):
		
		ValidExt = [self.ValidExt]
		
		#Collect a list of imported key files
		for TempExt in ValidExt:
			
			GlobString = os.path.join(self.ImportedKeysDir,"*."+TempExt)
			
			ListOfFiles = GlobAllFiles(GlobString);
			
			for FileInList in ListOfFiles:
				return True
				
		return False
		
	def CheckIfIdentifierKeysExist(self):
		
		for Identifier in self.IdentifiedKeyStore.keys():
			
			return True
			
		return False
	
	def TestDiffieHellmanKeyExchange(self):
		
		a = DiffieHellmanKeyExchange()
	
		a.GeneratePrivateKey()
		a.GeneratePublicKey()
			
		
		b = DiffieHellmanKeyExchange()
			
		b.GeneratePrivateKey()
		b.GeneratePublicKey()
			
			
		a.GenerateSecret(b.public_key)
		b.GenerateSecret(a.public_key)
			
		print(a.key)
		print(b.key)
			
		if a.key == b.key:
			print("It works!")
			return True
		else:
			print("Ooooops...")
			return False
