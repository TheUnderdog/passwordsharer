#Comment to stop GitLab deleting this stuff

# PasswordSharer

Prototypal proof-of-concept of DiffieHellman key based shared secrets.

**Requirements**

- Python 3.5 or newer
- A decent CPU for prime number generation
- Patience

**Features**

- Shared Key Management
Load keys of specific people you want to interact with.

- Key Forking
Generate derivative, temporary keys that even if exposed, do not expose the original key.
Supports customisation of everything, from temporal precision, to manual date/time setting, number of iterations, number of XOR folds per cycle, etc.
Permits user to input their own password to generate a fork of, or use the pre-loaded shared secret.

- Temporal Salt
Use the date/time to generate an expiry 'salt' added to the input password.
Temporal Precision can be set, from years, months, days, hours, minutes and, even seconds.

- Hash Obscurantum
Input character dependent algorithm selection.
The longer the string, the more algorithms are applied.

- XOR Sandwiching
Fold down even 'bad' hashes with XOR to make unreversible hash strings.

- Hash Obscurantum Cycling
Apply numerous loops of alternating Hash Obscurantum and XOR Sandwiching feeding into itself to produce a hash to a desired length.
Suitable for One-Time-Pad operations.

- Temporal Hash Obscurantum
Feeds a Temporal Salt into Hash Obscurantum to produce a brute-force resistant output that 'expires'.

- Random Prime Length Generation
Generates a random prime anywhere between 16384 to 32768 bits on both ends to make length unguessable and require brute-forcing on the prime length space.

- Random On-The-Fly Prime Generation
Generates a random near-prime on both ends to prevent prime guessing/interception.

- Threaded Prime Generation
Prime Generation is threaded so survival blurbs can be displayed.

- Adaptive User Interface
User Interface only displays the options you're able to currently undertake.


**Bugs**

- If the survival advice blurb has too many characters, it prints to the second line, which Python does not reset properly with print's '\r' feature, resulting in a flood of text. As users can resize the terminal as they see fit, this is almost impossible to meaningfully resolve.
- Line reading of files is problematic between Windows/Linux handshakes.

**Insecurity Issues**

- KeyStore files are not encrypted and definitely should be.
- Keyloggers being used against this are a real possibility.
- Memory intercept is another possibility. XOR'd memory with hops might be required.

**TODO**

- Password prompt user on load to decrypt/encrypt KeyStore files.
- Find better fix for TextUI's print Issue.
- Add the ability to encrypt/decrypt binary file data (Windows/Linux incompatibility issues).
