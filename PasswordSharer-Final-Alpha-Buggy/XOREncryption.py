

class XOREncryption:
	
	def __init__(self):
		self.Encoding = 'cp437'
	
	def IsOdd(self,Incoming):

		return bool(Incoming & 1)

	def IsEven(self,Incoming):
		
		return not self.IsOdd(Incoming)
	
	def EncryptXOR(self,IncomingA,IncomingKey):
		TempListOfInts = [ a ^ b for (a,b) in zip(bytes(IncomingA, self.Encoding), bytes(IncomingKey, self.Encoding)) ]
		EncryptedString = [chr(i) for i in TempListOfInts]
		
		return "".join(EncryptedString)

	def DecryptXOR(self,IncomingA,IncomingKey):
		TempListOfInts = [ a ^ b for (a,b) in zip(bytes(IncomingA, self.Encoding), bytes(IncomingKey, self.Encoding)) ]
		DecryptedString = [chr(i) for i in TempListOfInts]
		
		return "".join(DecryptedString)

	def EncryptDecryptTest(self,IncomingString,IncomingKey):
		Encrypted = self.EncryptXOR(IncomingString,IncomingKey)
		Decrypted = self.DecryptXOR(Encrypted,IncomingKey)
		
		print("Encrypted: "+str(Encrypted))
		print("Decrypted: "+str(Decrypted))
		return Decrypted
	
	def FindNearestCompressionPoint(self, IncomingNumber):
		
		Iter = 2
		
		while Iter < IncomingNumber:
			
			Iter = Iter * 2
			
		return Iter
		
	def GetEndCharacterInt(self, IncomingString):
		
		return ord( IncomingString[ len(IncomingString) - 1 ] )
		
	def StringNormalisationOffset(self, IncomingString):
		
		TempLen = len(IncomingString)
		CompressionPoint = self.FindNearestCompressionPoint(TempLen)
		
		return CompressionPoint - TempLen
	
	def XORFoldsCheck(self,Folds,IncomingFolds):
		
		if IncomingFolds is None:
			return True
		
		return bool(Folds < IncomingFolds)
		
	def MaxLengthCheck(self,MaxLength,IncomingLength):
		
		if MaxLength == None:
			return True
			
		return IncomingLength > MaxLength
	
	def XORFold(self,IncomingString,IncomingFolds=256,MaxLength=None):
	
		TempString = "" + IncomingString
		OutByOneOffset = 1
		Folds = 0
		
		while self.XORFoldsCheck(Folds,IncomingFolds) and self.MaxLengthCheck(MaxLength,len(TempString)):
			
			if self.IsOdd(len(TempString)):
				print('Warning: String not divisible to this level. Length: '+str(len(TempString))+'. Exiting early.')
				break
				
			TempHalfLen = int(len(TempString) / 2)
			
			FirstHalf = TempString[ :TempHalfLen ]
			SecondHalf = TempString[ TempHalfLen: ]
			
			TempString = self.EncryptXOR(FirstHalf,SecondHalf)
			
			Folds = Folds + 1
		
		DataObject = {}
		
		DataObject["data"] = TempString
		DataObject["folds"] = Folds
		
		return DataObject
		

	def XORCompressString(self,IncomingString,IncomingFolds=None,MaxLength=50):
		
		DataObject = {}
		Offset = self.StringNormalisationOffset(IncomingString)
		
		if Offset < 0:
			raise ValueError('Offset is somehow less than 0')

		AppendString = ""
		
		if Offset != 0:
			DataObject["mode"] = "lesser"
			EndInt = (self.GetEndCharacterInt(IncomingString) + 1) % 128
			EndChar = chr(EndInt)
			
			AppendString = EndChar * Offset
		else:
			DataObject["mode"] = "exact"
			
		TempString = IncomingString + AppendString
		
		TempObject = self.XORFold(TempString,IncomingFolds,MaxLength)
		DataObject["folds"] = TempObject["folds"]
		DataObject["data"] = TempObject["data"]
		
		return DataObject
		
	def XORCompressStringSimple(self,IncomingString,IncomingFolds=None,MaxLength=50):
		
		TempDataObject = self.XORCompressString(IncomingString,IncomingFolds,MaxLength)
		
		return TempDataObject["data"].encode(self.Encoding).hex()
		
		
	def Blinding(self,IncomingString):
		
		TempString = ""+IncomingString
		AddString = ""
		
		Iter = 1
		for EachChar in TempString:
			
			TempOrd = ord(EachChar)
			ModulatedOffset = (TempOrd+Iter) % 128
			ModulatedChar = chr(ModulatedOffset)
			
			if (TempOrd % 2) == 0:
				AddString = ModulatedChar + AddString
			else:
				AddString = AddString + ModulatedChar
			
			Iter = Iter + (TempOrd % 3)
		
		
		if (len(TempString) % 2) == 0:
			TempString = TempString + AddString
		else:
			TempString = AddString + TempString
		
		return self.XORCompressStringSimple(TempString.encode(self.Encoding).hex(),1,None)
