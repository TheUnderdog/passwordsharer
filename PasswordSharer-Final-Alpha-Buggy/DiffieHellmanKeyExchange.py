#Version 1.9

#TODO: Add fixable Timedate to enable prime hash synchronisation

import os
import ssl
import time
import struct
import hashlib

from TemporalSalt import *
from Crypto.Util import number
from TextUI import *
from HashObscurantum import *
from TemporalHashObscurantum import *

import threading

random_function = ssl.RAND_bytes


class DiffieHellmanKeyExchange:	
	
	def __init__(self,IncomingUseTemporalPassword=True,IncomingTemporalLevel=8,FixedTime=None):
		
		self.public_key = None
		self.key = None
		self.RandomLength = None
		self.RealLength = None
		self.Prime = None
		
		self.generator = 2
	
		self.GenerateRandomLengthSafe()
		
	
	def CryptoSecureRandomIntBetweenRange(self,StartInt,EndInt):
		
		Offset = EndInt - StartInt
		RandInt = struct.unpack('I', os.urandom(4))[0] % (Offset+1)
		return StartInt+RandInt
	
	
	def GenerateRandomLength(self):
		self.RandomLength = self.CryptoSecureRandomIntBetweenRange(100,1000)
	
	def GenerateRandomLengthSafe(self):
		
		if self.RandomLength is None:
			self.GenerateRandomLength()
	
	
	def GetPrimeTemplate(self,TargetLength,OptionalBox=None):
		
		TempPrime = number.getPrime(TargetLength,os.urandom)
		
		if isinstance(OptionalBox, list):
			OptionalBox.append(TempPrime)
		
		return TempPrime
		
	
	def GetPrime(self,TargetLength):
		
		ResultBox = []
		
		PrimeThread = threading.Thread(target=self.GetPrimeTemplate,args=(TargetLength,ResultBox))
		PrimeThread.start()
		
		#self.TUI.StartBlurbTimer()
		
		while PrimeThread.is_alive():
			pass
		
		#self.TUI.StopBlurbTimer()
		PrimeThread.join()
		
		return ResultBox[0]
		
	
	def GeneratePrimeAuto(self):
		
		self.GenerateRandomLengthSafe()
		self.Prime = self.GetPrime(self.RandomLength)
		self.RealLength = len(str(self.Prime))
	
	
	def GetRealLengthSafe(self):
		
		if self.RealLength is None:
			
			self.GeneratePrimeAuto()
		
		return self.RealLength
	
	def GetPrimeSafe(self):
		
		if self.Prime is None:
			
			self.GeneratePrimeAuto()
			
		return self.Prime
	
	def GeneratePrivateKey(self, TargetLength):
	
		_rand = 0
		_bytes = TargetLength // 8 + 8
		 
		while(_rand.bit_length() < TargetLength):
			_rand = int.from_bytes(random_function(_bytes), byteorder='big')
			  
		self.private_key = _rand
	
		 	
	def GeneratePublicKey(self):
		self.public_key = pow(self.generator, self.private_key, self.GetPrimeSafe())


	def GenerateSecret(self, public_key):
 
		self.shared_secret = pow(int(public_key), self.private_key, self.GetPrimeSafe())
		
		shared_secret_bytes = self.shared_secret.to_bytes(self.shared_secret.bit_length() // 8 + 1, byteorder='big')
		
		hash_alg = hashlib.sha256()
		
		hash_alg.update(bytes(shared_secret_bytes))
		
		self.key = hash_alg.hexdigest()
		
		return str(self.key)



if __name__ == '__main__':
	
	a = DiffieHellmanKeyExchange()
	
	print("Private A")
	#a.GeneratePrivateKey(a.GetRealLengthSafe())
	a.GeneratePrivateKey(a.RandomLength)
	print("Public A")
	a.GeneratePublicKey()
	
	b = DiffieHellmanKeyExchange()
	
	print("Private B")
	#b.GeneratePrivateKey(b.GetRealLengthSafe())
	b.GeneratePrivateKey(b.RandomLength)
	print("Public B")
	b.GeneratePublicKey()
		
	print("Generate Secret A")
	a.GenerateSecret(b.public_key)
	print("Generate Secret B")
	b.GenerateSecret(a.public_key)
		
	print(a.key)
	print(b.key)
		
	if a.key == b.key:
		print("Prime Generation Works")	
	else:
		print("ERROR in Prime Generation")
		
	exit()
