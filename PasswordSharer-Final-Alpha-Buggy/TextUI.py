import os
import struct
import threading
from FileIO import *

class TextUI:
	
	def __init__(self):
		self.MaxLength = 30
		self.BlurbList = None
		self.BlurbsFilename = os.path.join(os.path.dirname(os.path.abspath(__file__)),"Blurbs.txt")
		self.BlurbTimerOn = False
		
	def CryptoSecureRandomIntBetweenRange(self,StartInt,EndInt):
		
		Offset = EndInt - StartInt
		RandInt = struct.unpack('I', os.urandom(4))[0] % (Offset+1)
		return StartInt+RandInt
	
	def PrintOverwrite(self,IncomingString):
		
		StringLength = len(IncomingString)
		
		if StringLength  > self.MaxLength:
			self.MaxLength = StringLength
		
		PrintBlank = " " * self.MaxLength
		
		ColumnsSize = os.get_terminal_size().columns
		LinesSize = os.get_terminal_size().lines
		
		TempString = IncomingString+PrintBlank
		
		if len(TempString) >= 154:
			TempString = TempString[:ColumnsSize-1]
			
		ClearString = '\r'
		
		if len(TempString) >= 84:
			ClearString = ClearString + '\r'
		
		#print(IncomingString+PrintBlank,end=ClearString)
		print(IncomingString+PrintBlank)
	
	def PressEnterToContinue(self):
		self.PrintOverwrite("Press ENTER to continue\r\n")
		input("")
		
	def GetUserInputSafe(self,TextScreen):
		
		Choice = ""
		while Choice == "":
			
			
			try:
				Choice = input(TextScreen)
				break
				
			except:
				print("Warning: Invalid Input.")
				Choice = ""
			
		return Choice
		
	def GetUserPasswordSafe(self,TextScreen):
		
		Choice = ""
		while Choice == "":
			
			try:
				Choice = getpass.getpass(TextScreen)
				break
				
			except:
				print("Warning: Invalid Input.")
				Choice = ""
			
			
		return Choice
		
	def GetUserInputIntSafe(self,TextScreen):
		
		Choice = 0
		while True:
			
			
			try:
				Choice = int(input(TextScreen))
				break
			except:
				print("Warning: Invalid Input. Use only numbers!")
				Choice = 0
			
		return Choice
		
	def LoadBlurbs(self):
		
		if not isinstance(self.BlurbList,list):
			self.BlurbList = LoadFileLines(self.BlurbsFilename,True)
		
		
	def GetBlurb(self):
		
		if not isinstance(self.BlurbList,list):
			self.LoadBlurbs()
		
		Selection = self.CryptoSecureRandomIntBetweenRange(0,len(self.BlurbList)-1)
		return self.BlurbList[Selection]
			
			
	def ShowBlurb(self):
		
		self.PrintOverwrite(" "+self.GetBlurb())	
		
		
	def ContinueBlurbTimer(self):
		
		if self.BlurbTimerOn:
			self.ShowBlurb()
			BlurbTimer = threading.Timer(interval=10.0,function=self.ContinueBlurbTimer)
			BlurbTimer.start()
		
	def StartBlurbTimer(self):
		
		print("Did you know...?\r\n")
		self.BlurbTimerOn = True
		self.ContinueBlurbTimer()
		
	def StopBlurbTimer(self):
		self.BlurbTimerOn = False
