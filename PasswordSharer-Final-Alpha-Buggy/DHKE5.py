import os
import ssl
import time
import struct
import hashlib

from TemporalSalt import *
from Crypto.Util import number
from TextUI import *
from HashObscurantum import *
from TemporalHashObscurantum import *

import threading

random_function = ssl.RAND_bytes

G_PhaseVariance = 3

G_ListOfPrimes = [
					0xB10B8F96A080E01DDE92DE5EAE5D54EC52C99FBCFB06A3C69A6A9DCA52D23B616073E28675A23D189838EF1E2EE652C013ECB4AEA906112324975C3CD49B83BFACCBDD7D90C4BD7098488E9C219A73724EFFD6FAE5644738FAA31A4FF55BCCC0A151AF5F0DC8B4BD45BF37DF365C1A65E68CFDA76D4DA708DF1FB2BC2E4A4371,
					0xA4D1CBD5C3FD34126765A442EFB99905F8104DD258AC507FD6406CFF14266D31266FEA1E5C41564B777E690F5504F213160217B4B01B886A5E91547F9E2749F4D7FBD7D3B9A92EE1909D0D2263F80A76A6A24C087A091F531DBF0A0169B6A28AD662A4D18E73AFA32D779D5918D08BC8858F4DCEF97C2A24855E6EEB22B3B2E5,
					0x87A8E61DB4B6663CFFBBD19C651959998CEEF608660DD0F25D2CEED4435E3B00E00DF8F1D61957D4FAF7DF4561B2AA3016C3D91134096FAA3BF4296D830E9A7C209E0C6497517ABD5A8A9D306BCF67ED91F9E6725B4758C022E0B1EF4275BF7B6C5BFC11D45F9088B941F54EB1E59BB8BC39A0BF12307F5C4FDB70C581B23F76B63ACAE1CAA6B7902D52526735488A0EF13C6D9A51BFA4AB3AD8347796524D8EF6A167B5A41825D967E144E5140564251CCACB83E6B486F6B3CA3F7971506026C0B857F689962856DED4010ABD0BE621C3A3960A54E710C375F26375D7014103A4B54330C198AF126116D2276E11715F693877FAD7EF09CADB094AE91E1A1597,
					0xFFFFFFFFFFFFFFFFC90FDAA22168C234C4C6628B80DC1CD129024E088A67CC74020BBEA63B139B22514A08798E3404DDEF9519B3CD3A431B302B0A6DF25F14374FE1356D6D51C245E485B576625E7EC6F44C42E9A637ED6B0BFF5CB6F406B7EDEE386BFB5A899FA5AE9F24117C4B1FE649286651ECE45B3DC2007CB8A163BF0598DA48361C55D39A69163FA8FD24CF5F83655D23DCA3AD961C62F356208552BB9ED529077096966D670C354E4ABC9804F1746C08CA18217C32905E462E36CE3BE39E772C180E86039B2783A2EC07A28FB5C55DF06F4C52C9DE2BCBF6955817183995497CEA956AE515D2261898FA051015728E5A8AAAC42DAD33170D04507A33A85521ABDF1CBA64ECFB850458DBEF0A8AEA71575D060C7DB3970F85A6E1E4C7ABF5AE8CDB0933D71E8C94E04A25619DCEE3D2261AD2EE6BF12FFA06D98A0864D87602733EC86A64521F2B18177B200CBBE117577A615D6C770988C0BAD946E208E24FA074E5AB3143DB5BFCE0FD108E4B82D120A92108011A723C12A787E6D788719A10BDBA5B2699C327186AF4E23C1A946834B6150BDA2583E9CA2AD44CE8DBBBC2DB04DE8EF92E8EFC141FBECAA6287C59474E6BC05D99B2964FA090C3A2233BA186515BE7ED1F612970CEE2D7AFB81BDD762170481CD0069127D5B05AA993B4EA988D8FDDC186FFB7DC90A6C08F4DF435C93402849236C3FAB4D27C7026C1D4DCB2602646DEC9751E763DBA37BDF8FF9406AD9E530EE5DB382F413001AEB06A53ED9027D831179727B0865A8918DA3EDBEBCF9B14ED44CE6CBACED4BB1BDB7F1447E6CC254B332051512BD7AF426FB8F401378CD2BF5983CA01C64B92ECF032EA15D1721D03F482D7CE6E74FEF6D55E702F46980C82B5A84031900B1C9E59E7C97FBEC7E8F323A97A7E36CC88BE0F1D45B7FF585AC54BD407B22B4154AACC8F6D7EBF48E1D814CC5ED20F8037E0A79715EEF29BE32806A1D58BB7C5DA76F550AA3D8A1FBFF0EB19CCB1A313D55CDA56C9EC2EF29632387FE8D76E3C0468043E8F663F4860EE12BF2D5B0B7474D6E694F91E6DBE115974A3926F12FEE5E438777CB6A932DF8CD8BEC4D073B931BA3BC832B68D9DD300741FA7BF8AFC47ED2576F6936BA424663AAB639C5AE4F5683423B4742BF1C978238F16CBE39D652DE3FDB8BEFC848AD922222E04A4037C0713EB57A81A23F0C73473FC646CEA306B4BCBC8862F8385DDFA9D4B7FA2C087E879683303ED5BDD3A062B3CF5B3A278A66D2A13F83F44F82DDF310EE074AB6A364597E899A0255DC164F31CC50846851DF9AB48195DED7EA1B1D510BD7EE74D73FAF36BC31ECFA268359046F4EB879F924009438B481C6CD7889A002ED5EE382BC9190DA6FC026E479558E4475677E9AA9E3050E2765694DFC81F56E880B96E7160C980DD98EDD3DFFFFFFFFFFFFFFFFF
					]
GENERATOR = 2




class DiffieHellmanKeyExchange:
	
	def __init__(self,TestNumber=None):
		
		self.THO = TemporalHashObscurantum(DefaultIterations=IncomingIterations,FixedTime=IncomingFixedTime)
		
		#self.key_length = self.CryptoSecureRandomIntBetweenRange(1,10)
		self.key_length = 512
		 
		if TestNumber is None:
			self.primeO = G_ListOfPrimes[G_PhaseVariance % len(G_ListOfPrimes)]	
			self.primeR = self.GetPrime(self.key_length) + 0
			self.prime = self.GetPrimeTemplate(self.key_length)
			print(self.GetPrimeTemplate(64))
			
		else:
			self.prime = TestNumber
		 
		 
		print(self.prime)
		 
		self.generator = GENERATOR
		self.key = None
		 
	def CryptoSecureRandomIntBetweenRange(self,StartInt,EndInt):
		
		Offset = EndInt - StartInt
		RandInt = struct.unpack('I', os.urandom(4))[0] % (Offset+1)
		return StartInt+RandInt
		
		
	def getPrimeStable(self,IncomingIterations=None,OptionalOverrideLevel=None,IncomingFixedTime=None):
		
		LoopList = [1,3,7,9,-1,-3,-4,-9]
		LoopListLength = len(LoopList)
		LoopListIter = 0
		
		#1 3 7 9
		TargetPrime = self.THO.GetTemporalSaltHashedAsInt(OptionalOverrideLevel,IncomingFixedTime)+LoopList[LoopListIter]
		
		while True:
			
			if number.isPrime(TargetPrime):
			
				print("TargetPrime: "+str(TargetPrime))
				return TargetPrime
			
			TargetPrime = (TargetPrime * 2) + LoopList[LoopListIter % LoopListLength]
			LoopListIter = LoopListIter + 1	

		
	def GetPrimeTemplate(self,TargetLength,OptionalBox=None):
		
		print("TL: "+str(TargetLength))
		#TempPrime = number.getPrime(512,os.urandom)
		#TempPrime = number.getPrime(512,random_function)
		
		
		a = TemporalSalt()
		TempPrime = getPrimeStable(a.GetTemporalSaltAsInt(14))
		
		if isinstance(OptionalBox, list):
			OptionalBox.append(TempPrime)
		
		return TempPrime

		
	def GetPrime(self,TargetLength):
		
		ResultBox = []
		
		PrimeThread = threading.Thread(target=self.GetPrimeTemplate,args=(TargetLength,ResultBox))
		PrimeThread.start()
		
		#self.TUI.StartBlurbTimer()
		
		while PrimeThread.is_alive():
			pass
		
		#self.TUI.StopBlurbTimer()
		PrimeThread.join()
		
		return ResultBox[0]

		 
	def GeneratePrivateKey(self, length):
		_rand = 0
		_bytes = length // 8 + 8
		 
		while(_rand.bit_length() < length):
			_rand = int.from_bytes(random_function(_bytes), byteorder='big')
			  
		self.private_key = _rand

		 
	def GetPrivateKeyAsBytes(self):
		return self.private_key.to_bytes(2, byteorder='big')
		
	def SetPrivateKeyFromBytes(self,IncomingBytes):
		self.private_key = int.from_bytes(IncomingBytes, byteorder='big')

	def GetPublicKeyAsBytes(self):
		return self.public_key.to_bytes(2, byteorder='big')
	
	def SetPublicKeyFromBytes(self,IncomingBytes):
		self.public_key = int.from_bytes(IncomingBytes, byteorder='big')
		
	
	def GetPrivateKeyAsString(self):
		return str(self.private_key)
		
	def SetPrivateKeyFromString(self,IncomingString):
		self.private_key = int(IncomingString)

	def GetPublicKeyAsString(self):
		return str(self.public_key)
	
	def SetPublicKeyFromString(self,IncomingString):
		self.public_key = int(IncomingString)
	
	def GeneratePublicKey(self):
		self.public_key = pow(self.generator, self.private_key, self.prime)

	def GetGeneratedKeyAsString(self):
		return str(self.key)

	def GenerateSecret(self, public_key):
 
		self.shared_secret = pow(int(public_key), self.private_key, self.prime)
		shared_secret_bytes = self.shared_secret.to_bytes(self.shared_secret.bit_length() // 8 + 1, byteorder='big')
		hash_alg = hashlib.sha256()
		hash_alg.update(bytes(shared_secret_bytes))
		self.key = hash_alg.hexdigest()
		
		return str(self.key)


if __name__ == '__main__':
	
	a = DiffieHellmanKeyExchange()

	print("Private A")
	a.GeneratePrivateKey(a.key_length)
	print("Public A")
	a.GeneratePublicKey()
	
	b = DiffieHellmanKeyExchange()
	
	print("Private B")
	b.GeneratePrivateKey(b.key_length)
	print("Public B")
	b.GeneratePublicKey()
		
	print("Generate Secret A")
	a.GenerateSecret(b.public_key)
	print("Generate Secret B")
	b.GenerateSecret(a.public_key)
		
	print(a.key)
	print(b.key)
		
	if a.key == b.key:
		print("Prime Generation Works")
		
	else:
		print("ERROR in Prime Generation")
		
	exit()
