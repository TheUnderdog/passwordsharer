#Version 1.9

#TODO: Add fixable Timedate to enable prime hash synchronisation

import os
import ssl
import time
import struct
import hashlib

from TemporalSalt import *
from Crypto.Util import number
from TextUI import *
from HashObscurantum import *
from TemporalHashObscurantum import *

import threading

random_function = ssl.RAND_bytes

G_DebugIter = -20

class DiffieHellmanKeyExchange:	
	
	def __init__(self,IncomingUseTemporalPassword=True,IncomingTemporalLevel=8,FixedTime=None):
		
		
		self.HO = HashObscurantum()
		self.TUI = TextUI()
		
		self.UseTemporalPassword = IncomingUseTemporalPassword
		self.UseObscurantumMode = True
		
		self.PrimeLengthsList = []
		self.PrimeLengthsList.append(self.CryptoSecureRandomIntBetweenRange(100,1000))
		#self.PrimeLengthsList.append(self.CryptoSecureRandomIntBetweenRange(16384,32768))
		
		self.TemporalLevel = IncomingTemporalLevel
		self.generator = 2
		 
		self.prime = None
		self.key = None
		
		self.PrimeLength = None
		self.RealPrimeLength = None
		
		self.public_key = None
		self.private_key = None
		self.THO = None
		
		self.SetFixedTime(FixedTime)
		
		self.LoadPrimeLength()
	
	def SetFixedTime(self,FixedTime):
		
		self.FixedTime = FixedTime
		
		if self.FixedTime is not None or self.THO is None:
			self.THO = TemporalHashObscurantum(FixedTime=self.FixedTime)
		
		
	def LoadPrimeLength(self):
		
		if self.PrimeLength is None:
			
			TempClock = self.THO.GetTemporalSaltHashedAsInt(Level=self.TemporalLevel,OptionalOverrideDateTime=self.FixedTime)
			
			if self.UseTemporalPassword:
				self.PrimeLength = self.GetPrimeLength(TempClock)
			
			else:
				self.PrimeLength = self.GetPrimeLength()
			
		
	def GeneratePrimeInternalSafe(self):
		
		self.LoadPrimeLength()
		
		if self.prime is None:
		
			TempClock = self.THO.GetTemporalSaltHashedAsInt(Level=self.TemporalLevel,OptionalOverrideDateTime=self.FixedTime)
			
			if self.UseTemporalPassword:
				self.prime = self.GetPrimeModulated(TempClock)
			
			else:
				self.prime = self.GetPrimeModulated()
				
			self.RealPrimeLength = len(str(self.prime))
	
		
	def CryptoSecureRandomIntBetweenRange(self,StartInt,EndInt):
		
		Offset = EndInt - StartInt
		RandInt = struct.unpack('I', os.urandom(4))[0] % (Offset+1)
		return StartInt+RandInt
	
	def GetPrimeLength(self,Modulator):
		return self.PrimeLengthsList[Modulator % len(self.PrimeLengthsList)]
	
	def GetPrime(self,Length):
		
		self.TUI.PrintOverwrite("Generating Prime (may take a while)...\r\n")
		
		global G_DebugIter
		
		if G_DebugIter > 0:
			raise ValueError('Swoop')
		
		G_DebugIter = G_DebugIter + 1
		
		ResultBox = []
		
		PrimeThread = threading.Thread(target=self.GetPrimeGeneric,args=(Length,ResultBox))
		PrimeThread.start()
		
		self.TUI.StartBlurbTimer()
		
		while PrimeThread.is_alive():
			pass
		
		self.TUI.StopBlurbTimer()
		PrimeThread.join()
		
		return ResultBox[0]
		
	def GetPrimeGeneric(self,Length,OptionalBox=None):
		
		TempPrime = number.getPrime(Length,os.urandom)
		
		if isinstance(OptionalBox, list):
			OptionalBox.append(TempPrime)
		
		return TempPrime
		
	
	def GetPrimeModulated(self,PrimeModulator=0):
		
		self.LoadPrimeLength()
		return self.GetPrime(self.PrimeLength)
	
	
	def GeneratePrivateKey(self, IncomingLength=None):
		
		if self.private_key is None:
			
			self.TUI.PrintOverwrite("Generating Private Key...")
			
			self.GeneratePrimeInternalSafe()
			
			if IncomingLength is None:
				length = self.RealPrimeLength
				
			else:
				length = IncomingLength
			
			_rand = 0
			_bytes = length // 8 + 8
			 
			while(_rand.bit_length() < length):
				_rand = int.from_bytes(random_function(_bytes), byteorder='big')
			
			if self.UseTemporalPassword:
				_rand = _rand * self.THO.GetTemporalSaltHashedAsInt(Level=self.TemporalLevel,OptionalOverrideDateTime=self.FixedTime)
			
			self.private_key = _rand
			
			self.TUI.PrintOverwrite("Private Key Generated.")
		 
		 
	def GetPrivateKeyAsBytes(self):
		return self.private_key.to_bytes(2, byteorder='big')
		
	def SetPrivateKeyFromBytes(self,IncomingBytes):
		self.private_key = int.from_bytes(IncomingBytes, byteorder='big')

	def GetPublicKeyAsBytes(self):
		return self.public_key.to_bytes(2, byteorder='big')
	
	def SetPublicKeyFromBytes(self,IncomingBytes):
		self.public_key = int.from_bytes(IncomingBytes, byteorder='big')
		
	
	def GetPrivateKeyAsString(self):
		return str(self.private_key)
		
	def SetPrivateKeyFromString(self,IncomingString):
		self.private_key = int(IncomingString)

	def GetPublicKeyAsString(self):
		return str(self.public_key)
	
	def SetPublicKeyFromString(self,IncomingString):
		self.public_key = int(IncomingString)
	
	def GeneratePublicKey(self):
		
		if self.public_key is None:
			
			self.TUI.PrintOverwrite("Generating Public Key...")
			
			self.GeneratePrimeInternalSafe()
			self.GeneratePrivateKey()
			self.public_key = pow(self.generator, self.private_key, self.prime)
			
			self.TUI.PrintOverwrite("Public Key Generated.")


	def GetGeneratedKeyAsString(self):
		return str(self.key)

	def GenerateSecret(self, IncomingPublicKey):
		
		self.TUI.PrintOverwrite("Generating Secret...")
		
		self.GeneratePrimeInternalSafe()
		self.GeneratePrivateKey()
		
		self.shared_secret = pow(int(IncomingPublicKey), self.private_key, self.prime)
		shared_secret_bytes = self.shared_secret.to_bytes(self.shared_secret.bit_length() // 8 + 1, byteorder='big')
		
		decoded_secret_bytes = shared_secret_bytes.decode('cp437')
		
		self.key = self.HO.ObscurantumHashCycler(decoded_secret_bytes)
		
		self.TUI.PrintOverwrite("Secret Generated.")
		return str(self.key)
