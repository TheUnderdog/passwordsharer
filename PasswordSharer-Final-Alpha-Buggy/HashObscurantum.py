import hashlib
import ssl
import time
from XOREncryption import *

class HashObscurantum:
	
	def __init__(self,DefaultIterations=112421):
		
		self.Encoding = 'cp437'
		
		self.XOREncryptor = XOREncryption()
		self.ResetJuggles()
		self.MinJuggle = len(self.JuggleListA)+1
		self.NonPBKDF2List = [
								"md5",
								"sha256",
								"sha384",
								"sha512",
							]
		
		self.IterationAmount = DefaultIterations
						
	def GetNonPBKDF2ListByLength(self,IncomingString):
		
		return self.NonPBKDF2List[ len(IncomingString) % len(self.NonPBKDF2List) ]
		
	def ResetJuggles(self):
		
		self.JuggleListA = [
								"pbkdf2_random",
								"pbkdf2_sha256",
								"pbkdf2_sha384",
								"pbkdf2_sha512",
								"md5",
								"sha256",
								"sha384",
								"sha512",
							]
							
		self.JuggleListB = []
		
	def DiagnosticSelfTest(self):
		
		DiagList = []
		
		for EachItem in self.JuggleListA:
			
			try:
				self.ObscurantumHashSelector("Test",EachItem)
				
				print(str(EachItem) + " available")
			except Exception as e:
				print(str(EachItem) + " NOT available: "+str(e))
				

	def IsOdd(self,Incoming):

		return bool(Incoming & 1)

	def IsEven(self,Incoming):
		
		return not self.IsOdd(Incoming)

	def ObscurantumHashSelector(self,IncomingStringToTransform,SelectorAsString='sha512',IncomingStringMode=None, IncomingIterationAmount = None):

		StringMode = self.Encoding
		
		if IncomingStringMode is not None:
			StringMode = IncomingStringMode

		IterationAmount = IncomingIterationAmount

		if IncomingIterationAmount is None:
			IterationAmount = self.IterationAmount

		if SelectorAsString == "shake_256":
			HA1 = hashlib.shake_256()
		
		elif SelectorAsString == "shake_128":
			HA1 = hashlib.shake_128()
			
		elif SelectorAsString == "pbkdf2_random" or SelectorAsString == "pbkdf2_hmac":
			
			A1 = self.GetNonPBKDF2ListByLength(IncomingStringToTransform)
			OH = self.ObscurantumHashSelector(IncomingStringToTransform,A1)
			A2 = self.GetNonPBKDF2ListByLength(OH)
		
			return hashlib.pbkdf2_hmac(A2,bytes(IncomingStringToTransform,StringMode), bytes(OH,self.Encoding), IterationAmount).hex()
		
		elif SelectorAsString == "pbkdf2_sha128":
			
			A1 = self.GetNonPBKDF2ListByLength(IncomingStringToTransform)
			OH = self.ObscurantumHashSelector(IncomingStringToTransform,A1)
			
			return hashlib.pbkdf2_hmac('sha128',bytes(IncomingStringToTransform,StringMode), bytes(OH,self.Encoding), IterationAmount).hex()

		elif SelectorAsString == "pbkdf2_sha256":
			
			A1 = self.GetNonPBKDF2ListByLength(IncomingStringToTransform)
			OH = self.ObscurantumHashSelector(IncomingStringToTransform,A1)
			
			return hashlib.pbkdf2_hmac('sha256',bytes(IncomingStringToTransform,StringMode), bytes(OH,self.Encoding), IterationAmount).hex()
		
		elif SelectorAsString == "pbkdf2_sha384":
			
			A1 = self.GetNonPBKDF2ListByLength(IncomingStringToTransform)
			OH = self.ObscurantumHashSelector(IncomingStringToTransform,A1)
			
			return hashlib.pbkdf2_hmac('sha384',bytes(IncomingStringToTransform,StringMode), bytes(OH,self.Encoding), IterationAmount).hex()
		
		
		elif SelectorAsString == "pbkdf2_sha512":
			
			A1 = self.GetNonPBKDF2ListByLength(IncomingStringToTransform)
			OH = self.ObscurantumHashSelector(IncomingStringToTransform,A1)
			
			return hashlib.pbkdf2_hmac('sha512',bytes(IncomingStringToTransform,StringMode), bytes(OH,self.Encoding), IterationAmount).hex()
			
		elif SelectorAsString == "blake2b":
			
			HA1 = hashlib.blake2b()
			
		#elif SelectorAsString == "blake2s":
			
			#HA1 = hashlib.blake2s()
		
		elif SelectorAsString == "md5":
			
			HA1 = hashlib.md5()
			
		elif SelectorAsString == "sha128":
			
			HA1 = hashlib.sha128()
		
		elif SelectorAsString == "sha256":
			
			HA1 = hashlib.sha256()
		
		elif SelectorAsString == "sha384":
			
			HA1 = hashlib.sha384()
			
		elif SelectorAsString == "sha512":
			HA1 = hashlib.sha512()
		
		else:
			
			A1 = self.GetNonPBKDF2ListByLength(IncomingStringToTransform)
			OH = self.ObscurantumHashSelector(IncomingStringToTransform,A1)
			
			return argon2.argon2_hash(password=IncomingStringToTransform, salt=ObscurantumHashSelector(IncomingStringToTransform,'pbkdf2_sha512'), t=len(IncomingStringToTransform), m=32, p=1, buflen=512, argon_type=argon2.Argon2Type.Argon2_i).hex()
		
		HA1.update(bytes(IncomingStringToTransform,StringMode))
		
		return HA1.hexdigest()

	def ObscurantumHashCycler(self,IncomingString, MaxFolds=None, MaxLength=50, OverrideIterationAmount=None):
		
		self.ResetJuggles()
		
		TempString = self.XOREncryptor.Blinding(""+IncomingString)
		
		TempString = str( self.ObscurantumHashSelector( TempString, self.JuggleListA[ len(TempString) % len(self.JuggleListA) ], IncomingIterationAmount=OverrideIterationAmount ) )
		
		Iter = 0
		ISL = len(IncomingString)
		
		for EachChar in IncomingString:
			
			print("Generating Hash Cycle. Part "+str(Iter+1)+" of "+str(ISL)+".",end='\r')
			
			if len(self.JuggleListA) < 1:
				self.JuggleListA = self.JuggleListB
				self.JuggleListB = []
				
			SelectorAsString = self.JuggleListA.pop(ord(EachChar) % len(self.JuggleListA))
			self.JuggleListB.append(SelectorAsString)
			
			TempString = str(self.ObscurantumHashSelector(TempString,SelectorAsString,IncomingIterationAmount=OverrideIterationAmount)) + TempString
			
			
			Iter = Iter + 1

		if Iter < 1:
			raise ValueError("Insufficient number of hash cycle iterations to secure the string")
		
		return self.XOREncryptor.XORCompressStringSimple(TempString,MaxFolds,MaxLength)


	def CheckSubLength(self,SubLength):
		
		if SubLength is None:
			return True
		
		if SubLength < 1:
			return False
			
		return True
	
	def ObscurantumOneTimePadGenerator(self,IncomingKey,MinimumOTPLength,TargetFolds=4,TargetSubLength=None):
		
		TempString = ""+IncomingKey
		TempFolds = TargetFolds
		
		
		if TargetFolds is None and TargetSubLength is None:
			TempFolds = 4
		
		TempLen = len(TempString)
		
		if self.CheckSubLength(TargetSubLength):
			TempString = self.ObscurantumHashCycler(TempString,TempFolds,TargetSubLength)
			
		else:
			TempString = self.ObscurantumHashCycler(TempString,TempFolds,TempLen / 2)
		
		
		Iter = 0
		while len(TempString) < MinimumOTPLength:
			
			TempLen = len(TempString)
			
			if self.CheckSubLength(TargetSubLength):
				TempString = self.ObscurantumHashCycler(TempString,TempFolds,TargetSubLength) + TempString
				
			else:
				TempString = self.ObscurantumHashCycler(TempString,TempFolds,TempLen / 2) + TempString
			
			
			Iter = Iter + 1
	
		return TempString[:MiniumOTPLength]
